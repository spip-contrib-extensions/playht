# Plugin Play.ht pour Spip

> Ce plugin n'est plus maintenu depuis 2022.

Vocalisation des contenus Spip par le biais du service [Play.ht](https://play.ht)

Play.ht est un service (payant) permettant de vocaliser du texte, avec toutes sortes de voix et d'options possibles.

Ce plugin facilite son intégration dans Spip.
Il s'agit à ce jour d'une intégration limitée, toutes les possibilités offertes par Play.ht ne sont pas directement intégrées.
Il est toutefois facile de basculer vers le dashboard de Play.ht si l'on souhaite utiliser les fonctions avancées.

Le plugin est utilisable, mais encore en développement !
L'architecture et les fonctionnalités présentes sont encore susceptibles de changer, et il peut rester des bugs.

Ceci est un début de documentation (incomplet).

## Liens

* Dashboard : https://play.ht/login/
* Présentation du service : https://www.youtube.com/watch?v=OWAPMbqnzKg
* Documentation API publique : https://github.com/playht/text-to-speech-api

## Configuration

1. Configurer les clés d’API et cie
2. Sélectionner les types d'objet pouvant être vocalisés
3. Configurer si besoin les templates

Les templates permettent d'ajouter automatiquement du texte à chaque contenu vocalisé, par exemple pour ajouter le nom du site au début de chaque audio.

## Utilisation et intégration

**Générer un audio**

1. Cliquer sur le bouton pour lancer la vocalisation
2. Choisir les options souhaitées : voix, etc.
3. Vérifier l'état de la conversion jusqu'à obtenir le message de confirmation (opération manuelle pour l'instant, à automatiser par la suite)

**Intégrer le lecteur dans un contenu ou dans un squelette**

Un modèle permet d'intégrer facilement le lecteur où l'on veut, il suffit d'indiquer le type et le numéro de l'objet en paramètre.

Exemple dans le texte d'un contenu :
```
<playht|objet=article|id_objet=N>
``` 

Exemple dans un squelette :
```html
<BOUCLE_article(ARTICLES) {id_article}>
#MODELE{playht, objet=article, id_objet=#ID_ARTICLE}
</BOUCLE_article>
```

## Étendre et personnaliser

Todo : documenter pipelines, etc.

## Notes techniques

Les contenus audios sont référencés dans la table `spip_vocalisations`

Play.ht propose 2 APIs :

* Une API publique et documentée, mais pour l'instant trop limitée pour une utilisation dans ce plugin : https://github.com/playht/text-to-speech-api
* Une API plus complète mais non documentée. Celle-ci est développée et maintenue pour une utilisation dans le plugin pour Wordpress.

On est obligé d'utiliser la 2ème API, dont les endpoints et le fonctionnement ont été déduits du plugin pour Wordpress.