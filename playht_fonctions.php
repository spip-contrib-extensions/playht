<?php
/**
 * Fonctions utiles au plugin Play.ht
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Formater les balises des champs de la table spip_vocalisations
 *
 * @param string|null $valeur
 * @param string $champ
 * @return string
 */
function playht_champ(? string $valeur, string $champ) : ? string {

	include_spip('inc/PlayHt');
	$app = new \Spip\PlayHt\App;

	switch ($champ) {
		case 'voice':
			foreach ($app->listVoices() as $lang => $voices) {
				if (isset($voices[$valeur])) {
					$valeur = $voices[$valeur];
					break;
				}
			}
			break;
		case 'narration_style':
			$valeur = ($app->listNarrationStyles($valeur) ?: $valeur);
			break;
		case 'audio_duration':
			if ($valeur = round($valeur, 0)) {
				// En minutes
				if ($valeur < (60 * 60)) {
					$format = 'i\:s';
				// En heures
				} else {
					$format = 'H\:i\:s';
				}
				$valeur = date($format, mktime(0, 0, $valeur));
			}
			break;
		case 'template':
			$templates = ($app->config['templates'] ?? []);
			$valeur = ($templates[$valeur]['nom'] ?? $valeur);
			break;
	}

	return $valeur;
}

/**
 * Slug compatible avec les différentes versions de Spip / Bonux
 *
 * @param string $valeur
 * @return string
 */
function playht_slugify(string $valeur) : string {
	// Spip 4
	if (function_exists('identifiant_slug')) {
		$valeur = identifiant_slug($valeur);
	// Spip 3.x + Bonux
	} elseif (function_exists('slugify')) {
		$valeur = slugify($valeur);
	// Slug du pauvre
	} else {
		$valeur = preg_replace('/[^a-z0-9]+/i', '-', trim($valeur));
	}

	return $valeur;
}

/**
 * Copie de la fonction de Spip 4, car il y a un bug bloquant dans les versions précédentes :
 * parfois ça force un POST alors qu'on demande un GET.
 * On l'utilise en fallback dans la méthode \Spip\PlayHt\App->request()
 *
 * Pour le docblock, voir celui de la fonction originale
 */
function playht_recuperer_url($url, $options = array()) {
	// Conserve la mémoire de la méthode fournit éventuellement
	$methode_demandee = $options['methode'] ?? '';
	$default = array(
		'transcoder' => false,
		'methode' => 'GET',
		'taille_max' => null,
		'headers' => [],
		'datas' => '',
		'boundary' => '',
		'refuser_gz' => false,
		'if_modified_since' => '',
		'uri_referer' => '',
		'file' => '',
		'follow_location' => 10,
		'version_http' => _INC_DISTANT_VERSION_HTTP,
	);
	$options = array_merge($default, $options);
	// copier directement dans un fichier ?
	$copy = $options['file'];

	if ($options['methode'] == 'HEAD') {
		$options['taille_max'] = 0;
	}
	if (is_null($options['taille_max'])) {
		$options['taille_max'] = $copy ? _COPIE_LOCALE_MAX_SIZE : _INC_DISTANT_MAX_SIZE;
	}


	// Ajout des en-têtes spécifiques si besoin
	$head_add = '';
	if (!empty($options['headers'])) {
		foreach ($options['headers'] as $champ => $valeur) {
			$head_add .= $champ . ': ' . $valeur . "\r\n";
		}
		// ne pas le repasser a recuperer_url si on follow un location, car ils seront dans datas
		unset($options['entetes']);
	}

	if (!empty($options['datas'])) {
		list($head, $postdata) = prepare_donnees_post($options['datas'], $options['boundary']);
		$head .= $head_add;
		if (stripos($head, 'Content-Length:') === false) {
			$head .= 'Content-Length: ' . strlen($postdata) . "\r\n";
		}
		$options['datas'] = $head . "\r\n" . $postdata;
		if (
			strlen($postdata)
			and !$methode_demandee
		) {
			$options['methode'] = 'POST';
		}
	} elseif ($head_add) {
		$options['datas'] = $head_add . "\r\n";
	}

	// Accepter les URLs au format feed:// ou qui ont oublie le http:// ou les urls relatives au protocole
	$url = preg_replace(',^feed://,i', 'http://', $url);
	if (!tester_url_absolue($url)) {
		$url = 'http://' . $url;
	} elseif (strncmp($url, '//', 2) == 0) {
		$url = 'http:' . $url;
	}

	$url = url_to_ascii($url);

	$result = array(
		'status' => 0,
		'headers' => '',
		'page' => '',
		'length' => 0,
		'last_modified' => '',
		'location' => '',
		'url' => $url
	);

	// si on ecrit directement dans un fichier, pour ne pas manipuler en memoire refuser gz
	$refuser_gz = (($options['refuser_gz'] or $copy) ? true : false);

	// ouvrir la connexion et envoyer la requete et ses en-tetes
	list($handle, $fopen) = init_http(
		$options['methode'],
		$url,
		$refuser_gz,
		$options['uri_referer'],
		$options['datas'],
		$options['version_http'],
		$options['if_modified_since']
	);
	if (!$handle) {
		spip_log("ECHEC init_http $url", 'distant' . _LOG_ERREUR);

		return false;
	}

	// Sauf en fopen, envoyer le flux d'entree
	// et recuperer les en-tetes de reponses
	if (!$fopen) {
		$res = recuperer_entetes_complets($handle, $options['if_modified_since']);
		if (!$res) {
			fclose($handle);
			$t = @parse_url($url);
			$host = $t['host'];
			// Chinoisierie inexplicable pour contrer
			// les actions liberticides de l'empire du milieu
			if (!need_proxy($host)
				and $res = @file_get_contents($url)
			) {
				$result['length'] = strlen($res);
				if ($copy) {
					ecrire_fichier($copy, $res);
					$result['file'] = $copy;
				} else {
					$result['page'] = $res;
				}
				$res = array(
					'status' => 200,
				);
			} else {
				spip_log("ECHEC chinoiserie $url", 'distant' . _LOG_ERREUR);
				return false;
			}
		} elseif ($res['location'] and $options['follow_location']) {
			$options['follow_location']--;
			fclose($handle);
			include_spip('inc/filtres');
			$url = suivre_lien($url, $res['location']);
			spip_log("recuperer_url recommence sur $url", 'distant');

			return recuperer_url($url, $options);
		} elseif ($res['status'] !== 200) {
			spip_log('HTTP status ' . $res['status'] . " pour $url", 'distant');
		}
		$result['status'] = $res['status'];
		if (isset($res['headers'])) {
			$result['headers'] = $res['headers'];
		}
		if (isset($res['last_modified'])) {
			$result['last_modified'] = $res['last_modified'];
		}
		if (isset($res['location'])) {
			$result['location'] = $res['location'];
		}
	}

	// on ne veut que les entetes
	if (!$options['taille_max'] or $options['methode'] == 'HEAD' or $result['status'] == '304') {
		return $result;
	}


	// s'il faut deballer, le faire via un fichier temporaire
	// sinon la memoire explose pour les gros flux

	$gz = false;
	if (preg_match(",\bContent-Encoding: .*gzip,is", $result['headers'])) {
		$gz = (_DIR_TMP . md5(uniqid(mt_rand())) . '.tmp.gz');
	}

	// si on a pas deja recuperer le contenu par une methode detournee
	if (!$result['length']) {
		$res = recuperer_body($handle, $options['taille_max'], $gz ? $gz : $copy);
		fclose($handle);
		if ($copy) {
			$result['length'] = $res;
			$result['file'] = $copy;
		} elseif ($res) {
			$result['page'] = &$res;
			$result['length'] = strlen($result['page']);
		}
		if (!$result['status']) {
			$result['status'] = 200; // on a reussi, donc !
		}
	}
	if (!$result['page']) {
		return $result;
	}

	// Decompresser au besoin
	if ($gz) {
		$result['page'] = implode('', gzfile($gz));
		supprimer_fichier($gz);
	}

	// Faut-il l'importer dans notre charset local ?
	if ($options['transcoder']) {
		include_spip('inc/charsets');
		$result['page'] = transcoder_page($result['page'], $result['headers']);
	}

	return $result;
}
