<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function playht_apercu_texte($objet, $id_objet, $template = '') {
	include_spip('inc/PlayHt');
	$id_objet = intval($id_objet);
	$app = new \Spip\PlayHt\App;
	$text = $app->getTextObject($objet, $id_objet, ['template' => $template]);
	$text = implode("\n\n", $text['content']);
	return $text;
}