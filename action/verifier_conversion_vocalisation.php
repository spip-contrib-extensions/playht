<?php
/**
 * Utilisation de l'action vérifier la conversion d'une vocalisation
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Action pour vérifier la conversion d'une vocalisation
 *
 * Si ok, on met à jour le statut + URL + durée de l'audio
 *
 * @param int|null $id_vocalisation
 *     Chaîne sous la forme <objet>-<id_objet>.
 *     Si absent, utilise l'argument de l'action sécurisée.
 * @param bool|null $json_out
 *     Indique si on veut afficher le retour sous forme de json
 * @return array|void
 *     Retourne soit un tableau associatif avec indication de l'état de la conversion, soit rien et produit un json
 *     - conversion en cours  : clé `converted` = 0
 *     - conversion terminée  : clé `converted` = 1 + `audioUrl`, `audioDuration` et `voice`
 *     - conversion en erreur : clé `converted` = 2 + `error`
 *     - conversion inconnue  : clé `converted` = 3 + `error`
 */
function action_verifier_conversion_vocalisation_dist(? int $id_vocalisation = null, ? bool $json_out = null) {

	include_spip('base/abstract_sql');
	include_spip('inc/autoriser');

	// Chercher les params dans le POST si absents
	// (on ne sécurise pas avec securiser_action, car l'appel peut provenir d'un script JS)
	if (is_null($id_vocalisation)) {
		$id_vocalisation = intval(_request('id_vocalisation'));
	}
	if (is_null($json_out)) {
		$json_out =(_request('json_out') ?? false);
	}

	$retour = [
		'converted' => 3, // Par défaut, on ne sait pas
	];
	$error = '';
	$set = [];

	$transcription_id = sql_getfetsel(
		'transcription_id',
		'spip_vocalisations',
		[
			'id_vocalisation = ' . intval($id_vocalisation),
			'statut = ' . sql_quote('conversion'),
		]
	);

	// Vérifs préliminaires
	if ($transcription_id === false) {
		$error = 'Aucune vocalisation en cours de conversion';
	} elseif (!strlen($transcription_id)) {
		$error = 'Champ transcription_id manquant';
	} elseif (!autoriser('modifier', 'vocalisation', $id_vocalisation)) {
		$error = 'Non autorisé';
	} else {
		try {
			include_spip('inc/PlayHt');
			$app = new \Spip\PlayHt\App;
			$response = $app->requestArticleStatus($transcription_id);
		}
		catch (\Exception $e) {
			$error = $e->getMessage();
		}
		finally {
			$retour = $response;
			$converted = ($response['converted'] ?? null);
			// Conversion finie : on change le statut + url et durée de l'audio
			if ($converted === 1) {
				$set = [
					'statut'         => 'publie',
					'audio_url'      => ($response['audioUrl'] ?? null),
					'audio_duration' => ($response['audioDuration'] ?? null),
				];
			// Conversion en erreur : on change le statut
			} elseif ($converted === 2) {
				$set = [
					'statut' => 'erreur',
				];
			}
			// Mise à jour de la vocalisation
			if ($set) {
				include_spip('action/editer_objet');
				objet_modifier('vocalisation', $id_vocalisation, $set);
			}
			// Erreurs
			if ($converted === 2 or $converted === 3) {
				$error = ($response['error'] ?? 'Erreur inconnue');
			}
		}
	}

	if ($error) {
		$retour['error'] = $error;
		spip_log("action_verifier_conversion_vocalisation : $error, id_vocalisation : $id_vocalisation", 'playht' . _LOG_ERREUR);
	}

	if ($json_out) {
		header('Content-Type: application/json');
		echo json_encode($retour);
	}

	return $retour;
}
