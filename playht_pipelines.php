<?php
/**
 * Utilisations de pipelines par Play.ht
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function playht_optimiser_base_disparus($flux) {

	sql_delete('spip_vocalisations', "statut='poubelle' AND maj < " . $flux['args']['date']);

	return $flux;
}


/**
 * Ajouter du contenu 
 *
 * Formulaire playht sur les objets configurés.
 *
 * @pipeline afficher_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function playht_affiche_milieu($flux) {

	$e         = trouver_objet_exec($flux['args']['exec']);
	$objet     = ($e['type'] ?? null);
	$cle_objet = ($e['id_table_objet'] ?? null);
	$id_objet  = ($flux['args'][$cle_objet] ?? null);
	$edition   = ($e['edition'] ?? false);

	if (
		$e !== false // page d'un objet éditorial
		and $edition === false // pas en mode édition
		and $objet
		and $id_objet
		and autoriser('vocaliserobjet', $objet, $id_objet)
	) {

		$texte = recuperer_fond(
			'prive/squelettes/editer/playht_objet',
			['objet' => $objet, 'id_objet' => $id_objet],
			['ajax' => true]
		);

		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}


/**
 * Après la mise à jour d'un contenu, vérifier si la vocalisation publiée est à jour
 *
 * @param [type] $flux
 * @return array
 */
function playht_post_edition(array $flux) : array {

	include_spip('base/abstract_sql');
	$action   = ($flux['args']['action'] ?? null);
	$objet    = ($flux['args']['type'] ?? null);
	$id_objet = ($flux['args']['id_objet'] ?? null);
	$modifs   = !empty($flux['data']);

	if (
		$action === 'modifier'
		and $modifs
		and autoriser('vocaliserobjet', $objet, $id_objet)
		and $vocalisation = sql_fetsel(
			'id_vocalisation,sync',
			'spip_vocalisations',
			[
				'objet = ' . sql_quote($objet),
				'id_objet = ' . intval($id_objet),
				'statut != ' . sql_quote('poubelle'),
			]
		)
	) {
		include_spip('inc/PlayHt');
		$app = new \Spip\PlayHt\App;
		$id_vocalisation = $vocalisation['id_vocalisation'];
		$sync = $vocalisation['sync'];
		$new_sync = ($app->checkHashObject($objet, $id_objet) ? '1' : '0');
		if ($new_sync !== $sync) {
			include_spip('action/editer_objet');
			objet_modifier('vocalisation', $id_vocalisation, ['sync' => $new_sync]);
		}
	}

	return $flux;
}
