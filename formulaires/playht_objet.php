<?php
/**
 * Gestion du formulaire de d'édition de vocalisation
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/PlayHt');
// include_spip('inc/actions');
// include_spip('inc/editer');

/**
 * Saisies
 *
 * @param string $objet
 *     Type d'objet associé.
 * @param int|string $id_objet
 *     Identifiant de l'objet associé.
 * @return string
 *     Hash du formulaire
 */
function formulaires_playht_objet_saisies_dist(string $objet = '', int $id_objet = 0) {

	$vocalisation = sql_fetsel(
		'voice, narration_style, global_speed, template',
		'spip_vocalisations',
		[
			'objet = ' . sql_quote($objet),
			'id_objet = ' . intval($id_objet),
			'statut != ' . sql_quote('poubelle'),
		]
	);

	include_spip('inc/filtres');
	$app = new \Spip\PlayHt\App;
	$repo = new \Spip\PlayHt\Repository;

	// Pour les voix, utiliser la langue de l'objet, sinon celle du site
	$lang = $app->getLangObject($objet, $id_objet);
	$voix_langs_defaut = lire_config('playht/voices');
	$voix_defaut = ($voix_langs_defaut[$lang] ?? null);

	// Templates
	$templates = [];
	foreach (lire_config('playht/templates') as $id_template => $val) {
		$templates[$id_template] = $val['nom'];
	}

	$saisies = [
		[
			'saisie' => 'selection',
			'options' => [
				'nom'                  => 'voice',
				'label'                => _T('vocalisation:champ_voice_label'),
				'defaut'               => ($vocalisation['voice'] ?? $voix_defaut),
				'data'                 => $app::listVoices($lang),
				'class'                => 'select2',
				'conteneur_class'      => 'long_label',
				'masquer_option_intro' => true,
			],
		],
		[
			'saisie' => 'selection',
			'options' => [
				'nom'                  => 'narration_style',
				'label'                => _T('vocalisation:champ_narration_style_label'),
				'defaut'               => ($vocalisation['narration_style'] ?? lire_config('playht/narration_style')),
				'data'                 => $app::listNarrationStyles(),
				'class'                => 'select2',
				'conteneur_class'      => 'long_label',
				'masquer_option_intro' => true,
			]
		],
		/*
		[
			'saisie' => 'input',
			'options' => [
				'nom'         => 'global_speed',
				'label'       => _T('vocalisation:champ_global_speed_label'),
				'explication' => _T('vocalisation:champ_global_speed_explication'),
				'defaut'      => ($vocalisation['global_speed'] ?? lire_config('playht/global_speed')),
				'type'        => 'number',
				'min'         => 20,
				'max'         => 200,
				'step'        => 10,
				'conteneur_class'      => 'long_label',
			]
		],
		*/
		[
			'saisie' => 'selection',
			'options' => [
				'nom'    => 'template',
				'label'  => _T('vocalisation:champ_template_label'),
				'data'   => $templates,
				'defaut' => ($vocalisation['template'] ?? null),
				'conteneur_class'=> 'long_label',
			],
		],
	];

	// Indication s'il n'y a pas de voix pour cette langue
	$langues_spip_ok = array_keys($repo::getLangsSpip());
	if (!in_array($lang, $langues_spip_ok)) {
		$saisies[1]['options']['explication'] = _T('playht:cfg_erreur_langues_notok');
	}

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param string $objet
 *     Type d'objet associé.
 * @param int|string $id_objet
 *     Identifiant de l'objet associé.
 * @return string
 *     Hash du formulaire
 */
function formulaires_playht_objet_identifier_dist(string $objet = '', int $id_objet = 0) {
	return serialize([$objet,intval($id_objet)]);
}

/**
 * Chargement du formulaire
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param string $objet
 *     Type d'objet associé.
 * @param int|string $id_objet
 *     Identifiant de l'objet associé.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_playht_objet_charger_dist(string $objet = '', int $id_objet = 0) {
	$valeurs = [
		'editer'   => '',
		'objet'    => $objet,
		'id_objet' => $id_objet,
		'id_vocalisation' => '',
	];

	return $valeurs;
}

/**
 * Vérifications du formulaire
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @param string $objet
 *     Type d'objet associé.
 * @param int|string $id_objet
 *     Identifiant de l'objet associé.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_playht_objet_verifier_dist(string $objet = '', int $id_objet = 0) {
	$erreurs = [];
	return $erreurs;
}

/**
 * Traitement du formulaire
 *
 * Traiter les champs postés
 *
 * @param string $objet
 *     Type d'objet associé.
 * @param int|string $id_objet
 *     Identifiant de l'objet associé.
 * @return array
 *     Retours des traitements
 */
function formulaires_playht_objet_traiter_dist(string $objet = '', int $id_objet = 0) {
	$retours = [
		'editable' => true,
	];

	$id_vocalisation = intval(_request('id_vocalisation'));

	// Convertir = créer ou mettre à jour un "article" chez Play.ht, puis lancer sa conversion en audio
	// On créé ou met à jour l'objet associé en local, avec le statut "en cours de conversion"
	if (_request('transcribe')) {

		$options = [
			'voice'            => _request('voice'),
			'narrationStyle'   => _request('narration_style'),
			// 'globalSpeed'      => _request('global_speed'),
			// 'readAlongEnabled' => _request('enable_read_along'),
			'template'         => _request('template'),
		];

		$app = new Spip\PlayHt\App();
		$id_objet = intval($id_objet);
		$convert = $app->convertObject($objet, $id_objet, $options);

		// L'audio distant est en cours de création : on crée ou met à jour l'objet localement, avec le statut en cours de conversion
		if (isset($convert['set'])) {

			include_spip('action/editer_objet');
			include_spip('inc/filtres');
			$set = $convert['set'];

			// Soit on met à jour
			$error_update = null;
			if ($id_vocalisation = sql_getfetsel('id_vocalisation', 'spip_vocalisations', ['objet='.sql_quote($objet), "id_objet=$id_objet"])) {
				$update = objet_modifier('vocalisation', $id_vocalisation, $set);
				if (is_string($update) and strlen($update)) {
					$error_update = _('playht:message_erreur_modifier', ['erreur' => $update]);
				}
			// Soit on crée
			} else {
				if (!$id_vocalisation = objet_inserer('vocalisation', null, $set)) {
					$error_update = _('playht:message_erreur_inserer');
				}
			}

			if (!$error_update) {
				$retours['message_ok'] =
					_T('playht:message_ok_transcripting')
					. '<br>' . _T('playht:message_ok_transcripting_check')
				;
				$retours['id_vocalisation'] = $id_vocalisation;
			} else {
				$retours['message_erreur'] = $error_update;
				spip_log("$error_update, set : " . json_encode($set), 'playht' . _LOG_ERREUR);
			}

			// Échec de la création de l'audio distant
		} else {
			$retours['message_erreur'] = $convert['error'];
		}
	

	// Vérifier le statut d'une conversion
	// Si ok, on récupère l'URL de l'audio et on met à jour
	} elseif (_request('transcription_status')) {
		$verifier_conversion = charger_fonction('verifier_conversion_vocalisation', 'action');
		$reponse = $verifier_conversion($id_vocalisation);
		$converted = ($reponse['converted'] ?? null);

		// Conversion finie
		if ($converted === 1) {
			$retours['message_ok'] = _T('playht:message_ok_transcripting_finie');
		// Conversion en cours
		} elseif ($converted === 0) {
			$retours['message_ok'] = _T('playht:message_ok_transcripting_encours');
		// Conversion en erreur
		} elseif ($converted === 2) {
			$retours['message_erreur'] = _T('playht:message_erreur_transcripting', ['erreur' => $reponse['error'] ?? '']);
		// Conversion inconnue
		} else {
			$retours['message_erreur'] = ($reponse['error'] ?? _T('playht:message_erreur_transcripting_inconnu'));
		}

	// Supprimer
	// TODO : message de confirmation
	} elseif (_request('supprimer')) {
		$supprimer_vocalisation = charger_fonction('supprimer_vocalisation', 'action');
		$supprimer_vocalisation($id_vocalisation);
		$retours['message_ok'] = _T('playht:bouton_supprimer_post');
	}

	return $retours;
}
