<?php

/**
 * Déclaration des saisies de vocalisation
 *
 * @return array
 *     Liste des saisies
 */
function formulaires_configurer_playht_saisies_dist() {

	include_spip('inc/PlayHtApp');
	include_spip('inc/PlayHtRepository');
	$app = new \Spip\PlayHt\App;
	$repo = new \Spip\PlayHt\Repository;

	$saisies = [

		// Options
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_options',
				'label' => _T('playht:cfg_options_label'),
				'pliable' => true,
				// 'plie' => true,
			],
			'saisies' => [
				// Les saisies pour la voix par défaut sont ajoutées ici par la suite
				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'narration_style',
						'label' => _T('playht:cfg_narration_style_label'),
						'data' => $app::listNarrationStyles(),
						'class' => 'select2',
						'conteneur_class' => 'long_label',
					],
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'global_speed',
						'label' => _T('playht:cfg_global_speed_label'),
						'explication' => _T('vocalisation:champ_global_speed_explication'),
						'type' => 'number',
						'min' => 20,
						'max' => 200,
						'step' => 10,
						'conteneur_class' => 'long_label',
					],
				],
				/*
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'read_along_enabled',
						'label' => _T('playht:cfg_read_along_enabled_label'),
						'label_case' => _T('playht:cfg_read_along_enabled_label_case'),
						// 'explication' => _T('vocalisation:champ_read_along_enabled_explication'),
						'valeur_oui' => 1,
						'valeur_non' => 0,
						'conteneur_class' => 'long_label',
					],
				],
				*/
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'base_url',
						'label' => _T('playht:cfg_base_url_label'),
						'explication' => _T('playht:cfg_base_url_explication'),
					],
				],
			],
		],

		// Objets
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_objets',
				'label' => _T('playht:cfg_objets_label'),
				'pliable' => true,
				'plie' => true,
			],
			'saisies' => [
				[
					'saisie' => 'choisir_objets',
					'options' => [
						'nom' => 'objets',
						// 'label' => _T('playht:cfg_objets_label'),
						'explication' => _T('playht:cfg_objets_explication'),
						'exclus' => ['spip_vocalisations'],
						'conteneur_class' => 'pleine_largeur',
					],
				],
			],
		],

		// Identifiants
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_credentials',
				'label' => _T('playht:cfg_credentials_label'),
				'pliable' => true,
				'plie' => true,
			],
			'saisies' => [
				// Clé secrète
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'authorization',
						'label' => _T('playht:cfg_authorization_label'),
						'conteneur_class' => 'long_label',
						// 'cle_secrete' => true,
					],
				],
				// Identifiant utilisateur
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'userid',
						'label' => _T('playht:cfg_userid_label'),
						'conteneur_class' => 'long_label',
						// 'cle_secrete' => true,
					],
				],
				// Identifiant application
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'appid',
						'label' => _T('playht:cfg_appid_label'),
						'conteneur_class' => 'long_label',
						// 'cle_secrete' => true,
					],
				],
			],
		],
	];

	// Saisies pour les voix ajoutées au début du fieldset des options
	$langues_spip_ok = array_keys($repo::getLangsSpip());
	$langues_spip    = array_values(array_unique(array_filter(array_merge(
		[lire_config('langue_site')],
		explode(',', lire_config('langues_multilingue'))
	))));
	foreach ($langues_spip as $lang) {
		$label_lang = ($GLOBALS['codes_langues'][$lang] ?? $lang);
		$saisie = [
			'saisie' => 'selection',
			'options' => [
				'nom' => "voices[$lang]",
				'label' => _T('playht:cfg_voice_lang_label', ['lang' => $label_lang]),
				'data' => $app::listVoices($lang),
				'class' => 'select2',
				'obligatoire' => 'oui',
				'conteneur_class' => 'long_label',
			],
		];
		if (!in_array($lang, $langues_spip_ok)) {
			$saisie['options']['explication'] = _T('playht:cfg_erreur_langues_notok');
		}
		array_unshift($saisies[0]['saisies'], $saisie);
	}

	return $saisies;
}
