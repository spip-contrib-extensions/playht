<?php

/**
 * Déclaration des saisies de vocalisation
 *
 * @return array
 *     Liste des saisies
 */
function formulaires_configurer_templates_playht_saisies_dist() {

	$balise_img = charger_filtre('balise_img');

	$saisies = [];

	// Les templates existants
	$templates = lire_config('playht/templates', []);
	foreach ($templates as $id_template => $template) {
		$nom_template = (!empty($template['nom']) ? $template['nom'] : null);
		$saisies[] = [
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_template_' . $template['nom'],
				'label' => $template['nom'],
			],
			'saisies' => [
				// Nom du template en hidden
				[
					'saisie' => 'hidden',
					'options' => [
						'nom' => "templates[$id_template][nom]",
						'valeur' => $nom_template,
					],
				],
				// contenu du template
				[
					'saisie' => 'textarea',
					'options' => [
						'nom' => "templates[$id_template][template]",
						'rows' => 3,
						'class' => 'multilang',
						'conteneur_class' => 'pleine_largeur'
					],
				],
				// bouton pour supprimer
				[
					'saisie' => 'playht_bouton',
					'options' => [
						'nom' => 'supprimer_template',
						'label_bouton' => _T('playht:cfg_bouton_supprimer_template_label'),
						'valeur_bouton' => $id_template,
						'class' => 'btn btn_secondaire float-end',
					],
				],
			],
		];
	}

	// Ajout d'un template : soit le bouton d'ajout
	if (!_request('ajouter_template')) {
		$saisies[] = [
			'saisie' => 'playht_bouton',
			'options' => [
				'nom' => 'ajouter_template',
				'label_bouton' => $balise_img(chemin_image('add-16.png')) . ' ' ._T('playht:cfg_bouton_ajouter_template_label'),
				'valeur_bouton' => 'ajouter',
				'conteneur_class' => 'pleine_largeur',
				'class' => 'btn_ajouter-template btn_large btn_bloc',
			],
		];
	// Soit un fieldset 
	} else {
		$saisies[] = [
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_template_ajouter',
				'label' => $balise_img(chemin_image('add-16.png')) . ' ' . _T('playht:cfg_bouton_ajouter_template_label'),
			],
			'saisies' => [
				[
					'saisie' => 'input',
					'options' => [
						'nom' => "ajouter_template[nom]",
						'label' => _T('playht:cfg_template_nom_label'),
						// 'obligatoire' => 'oui',
					],
				],
				[
					'saisie' => 'textarea',
					'options' => [
						'nom' => "ajouter_template[template]",
						'label' => _T('playht:cfg_template_texte_label'),
						'explication' => _T('playht:cfg_template_texte_explication'),
						'defaut' => "@titre@\n\n@contenu@",
						// 'obligatoire' => 'oui',
						'rows' => 3,
						'class' => 'multilang',
					],
				]
			],
		];
	}

	return $saisies;
}

/**
 * Déclaration des valeurs au chargement du formulaire
 *
 * @return array
 *     Valeurs au chargement du formulaire
 */
function formulaires_configurer_templates_playht_charger_dist() {
	include_spip('inc/config');
	$valeurs = [
		'templates'          => _request('templates') ?: lire_config('playht/templates', []),
		'ajouter_template'   => _request('ajouter_template'),
		'supprimer_template' => _request('supprimer_template'),
	];

	return $valeurs;
}

/**
 * Vérifier les valeurs postées
 *
 * @return array
 *     Valeurs au chargement du formulaire
 */
function formulaires_configurer_templates_playht_verifier_dist() {
	$erreurs = [];

	// Valeurs obligatoires
	if (is_array($ajouter_template = _request('ajouter_template'))) {
		if (empty($ajouter_template['nom'])) {
			$erreurs['ajouter_template[nom]'] = _T('info_obligatoire');
		}
		if (empty($ajouter_template['template'])) {
			$erreurs['ajouter_template[template]'] = _T('info_obligatoire');
		}
	}

	return $erreurs;
}

/**
 * Traitements du formulaire
 *
 * @return array
 */
function formulaires_configurer_templates_playht_traiter_dist() {
	include_spip('inc/config');
	$retours = [
		'editable' => true,
	];

	$templates = _request('templates');

	// Suppression
	if ($id_supprimer = _request('supprimer_template')) {
		$nom = $templates[$id_supprimer]['nom'];
		unset($templates[$id_supprimer]);
		$retours['message_ok'] = _T('playht:message_ok_template_supprime', ['nom' => $nom]);
	}

	// Ajout
	if (
		is_array($ajouter_template = _request('ajouter_template'))
		and array_filter($ajouter_template)
	) {
		$nom = $ajouter_template['nom'];
		$template = $ajouter_template['template'];
		$id = playht_slugify($nom);
		$templates[$id] = [
			'nom' => $nom,
			'template' => $template,
		];
		set_request('ajouter_template', '');
		$retours['message_ok'] = _T('playht:message_ok_template_ajoute', ['nom' => $nom]);
	}

	ecrire_config('playht/templates', $templates);

	return $retours;
}
