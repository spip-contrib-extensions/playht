<?php

/**
 * Déclaration des saisies de vocalisation
 *
 * @return array
 *     Liste des saisies
 */
function formulaires_configurer_prononciations_playht_saisies_dist() {

	$saisies = [];

	// On convertit le tableau au format attendu par la saisie
	$pronunciations = [];
	$cfg_pronunciations = lire_config('playht/pronunciations', []);
	if (is_array($cfg_pronunciations)) {
		foreach ($cfg_pronunciations as $term => $pronunciation) {
			$pronunciations[] = [
				'term' => $term,
				'pronunciation' => $pronunciation,
			];
		}
	}
	// Ajout d'une ligne vide
	$pronunciations[] = [
		'term' => '',
		'pronunciation' => ''
	];

	$saisies[] = [
		'saisie' => 'playht_prononciations',
		'options' => [
			'nom' => 'pronunciations',
			// 'label' => _T('playht:cfg_pronunciations_label'),
			'explication' => _T('playht:cfg_pronunciations_explication'),
			// 'valeur' => _request('pronunciations'),
			'data' => $pronunciations,
			'conteneur_class' => 'pleine_largeur',
		],
	];

	return $saisies;
}

/**
 * Déclaration des valeurs au chargement du formulaire
 *
 * @return array
 *     Valeurs au chargement du formulaire
 */
function formulaires_configurer_prononciations_playht_charger_dist() {
	include_spip('inc/config');
	$valeurs = [
		'pronunciations' => _request('pronunciations'),
	];

	return $valeurs;
}

/**
 * Vérifier les valeurs postées
 *
 * @return array
 *     Valeurs au chargement du formulaire
 */
function formulaires_configurer_prononciations_playht_verifier_dist() {
	$erreurs = [];
	if ($pronunciations = array_filter(_request('pronunciations'))) {
		foreach ($pronunciations as $k => $set) {
			if (!empty($set['term']) or !empty($set['pronunciation'])) {
				if (empty($set['term'])) {
					$erreurs['pronunciations'][$k]['term'] = _T('info_obligatoire');
				}
				if (empty($set['pronunciation'])) {
					$erreurs['pronunciations'][$k]['pronunciation'] = _T('info_obligatoire');
				}
			}
		}
	}
	return $erreurs;
}

/**
 * Traitements du formulaire
 *
 * @return array
 */
function formulaires_configurer_prononciations_playht_traiter_dist() {
	include_spip('inc/config');
	include_spip('inc/PlayHtApp');
	$app = new \Spip\PlayHt\App;
	$retours = [
		'editable' => true,
	];
	$erreur = null;

	$pronunciations = [];
	if ($set_pronunciations = array_filter(_request('pronunciations'))) {
		foreach ($set_pronunciations as $set) {
			if (array_filter($set)) {
				$pronunciations[$set['term']] = $set['pronunciation'];
			}
		}
		ecrire_config('playht/pronunciations', $pronunciations);
		try {
			$app->requestUserPronunciations($pronunciations);
		} catch (\Exception $e) {
			$erreur = $e->getMessage();
			$retours['message_erreur'] = $erreur;
		}

		if (!$erreur) {
			$retours['message_ok'] = _T('config_info_enregistree');
		}
	}

	return $retours;
}
