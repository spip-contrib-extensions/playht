<?php
/**
 * Gestion du formulaire de d'édition de vocalisation
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de vocalisation
 *
 * @param int|string $id_vocalisation
 *     Identifiant du vocalisation. 'new' pour un nouveau vocalisation.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un vocalisation source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du vocalisation, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_vocalisation_saisies_dist($id_vocalisation = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'objet',
				'label' => _T('vocalisation:champ_objet_label'),
			),
		),

		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'id_objet',
				'label' => _T('vocalisation:champ_id_objet_label'),
			),
		),

		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'transcription_id',
				'label' => _T('vocalisation:champ_transcription_id_label'),
			),
		),

		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'article_id',
				'label' => _T('vocalisation:champ_article_id_label'),
			),
		),

		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'audio_url',
				'label' => _T('vocalisation:champ_audio_url_label'),
			),
		),

		array(
			'saisie' => 'playht_voice',
			'options' => array(
				'nom' => 'voice',
				'label' => _T('vocalisation:champ_voice_label'),
			),
		),

		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'audio_duration',
				'label' => _T('vocalisation:champ_audio_duration_label'),
			),
		),

		array(
			'saisie' => 'playht_narration',
			'options' => array(
				'nom' => 'narration_style',
				'label' => _T('vocalisation:champ_narration_style_label'),
			),
		),

		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'global_speed',
				'label' => _T('vocalisation:champ_global_speed_label'),
			),
		),

	);
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_vocalisation
 *     Identifiant du vocalisation. 'new' pour un nouveau vocalisation.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un vocalisation source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du vocalisation, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_vocalisation_identifier_dist($id_vocalisation = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_vocalisation)));
}

/**
 * Chargement du formulaire d'édition de vocalisation
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_vocalisation
 *     Identifiant du vocalisation. 'new' pour un nouveau vocalisation.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un vocalisation source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du vocalisation, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_vocalisation_charger_dist($id_vocalisation = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('vocalisation', $id_vocalisation, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	$valeurs['saisies'] = call_user_func_array('formulaires_editer_vocalisation_saisies_dist', func_get_args());
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de vocalisation
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_vocalisation
 *     Identifiant du vocalisation. 'new' pour un nouveau vocalisation.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un vocalisation source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du vocalisation, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_vocalisation_verifier_dist($id_vocalisation = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$erreurs = formulaires_editer_objet_verifier('vocalisation', $id_vocalisation);
	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de vocalisation
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_vocalisation
 *     Identifiant du vocalisation. 'new' pour un nouveau vocalisation.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un vocalisation source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du vocalisation, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_vocalisation_traiter_dist($id_vocalisation = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('vocalisation', $id_vocalisation, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
