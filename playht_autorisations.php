<?php
/**
 * Définit les autorisations du plugin Play.ht
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function playht_autoriser() {
}

/**
 * Autorisation de configurer le plugin
 *
 * Les admins et +
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_playht_configurer_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = ($qui['statut'] === '0minirezo');
	return $autoriser;
}


// ------------------
// Objet vocalisation


/**
 * Autorisation de voir les vocalisations
 *
 * Tout le monde 
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_vocalisations_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir une vocalisation
 *
 * Tout le monde
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_vocalisation_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de créer une vocalisation
 *
 * Admins et rédacteurs
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_vocalisation_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

/**
 * Autorisation de modifier une vocalisation
 *
 * Il faut avoir le droit de modifier l'objet associé
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_vocalisation_modifier_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = false;
	if ($vocalisation = sql_fetsel('objet, id_objet', 'spip_vocalisations', 'id_vocalisation='.intval($id))) {
		$autoriser = autoriser('modifier', $vocalisation['objet'], $vocalisation['id_objet']);
	}
	return $autoriser;
}

/**
 * Autorisation de supprimer une vocalisation
 *
 * Il faut avoir le droit de modifier et que le statut soit en erreur ou poubelle
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_vocalisation_supprimer_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = (
		autoriser('modifier', 'vocalisation', $id)
		and in_array(sql_getfetsel('statut', 'spip_vocalisations', 'id_vocalisation='.intval($id)), ['erreur', 'poubelle'])
	);

	return $autoriser;
}

/**
 * Autorisation d'utiliser le formulaire pour ajouter une vocalisation à un objet
 *
 * Il faut que l'objet soit activé dans la config OU qu'il y ait déjà une vocalisation
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_vocaliserobjet_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/config');
	include_spip('base/objets');
	include_spip('base/abstract_sql');
	$objets = lire_config('playht/objets');
	$table = table_objet_sql($type);
	$autoriser = (
		in_array($table, $objets)
		or sql_countsel('spip_vocalisations', ['objet ='.sql_quote($type), 'id_objet ='.intval($id), 'statut !='.sql_quote('poubelle')])
	);

	return $autoriser;
}
