<?php

namespace Spip\PlayHt;
use Spip\PlayHt\Repository;

/**
 * Class PlayHt
 *
 * Implémente l'API de Play.ht
 *
 * Nb : il y a une API publique documentée, mais elle assez limitée :
 * elle permet de créer un audio et récupérer son URL, mais ensuite on n'a plus la main dessus,
 * toute modif ultérieure doit se faire dans le dashboard de Play.ht
 *
 * On implémente l'API "officieuse" bien plus complète employée dans le plugin pour Wordpress,
 * qui permet bien plus de choses.
 *
 * @see https://github.com/playht/text-to-speech-api
 */
class App {

	CONST BASE_URL = 'https://play.ht/api/';

	CONST ENDPOINTS = [
		'setArticleInfo'             => 'setArticleInfo',
		'transcripe'                 => 'transcripe',
		'user-credits'               => 'user-credits',
		'user-conf'                  => 'user-conf',
		'user-pronunciations'        => 'user-pronunciations',
		'getArticleAudioObjectByURL' => 'getArticleAudioObjectByURL',
		'addOrigin'                  => 'addOrigin',
		'saveReferralSource'         => 'plugin/saveReferralSource',
		'convert'                    => 'v1/convert',
		'articleStatus'              => 'v1/articleStatus',
	];

	public $config;

	public $authorization;

	public $userId;

	public $appId;

	public $defaults = [];

	/**
	 * Constructeur
	 */
	public function __construct() {
		include_spip('inc/utils'); // pour les spip_log
		$this->setConfig();
		$this->setCredentials();
		$this->setDefaults();
	}


	// =================
	// Requêtes de l'API
	// ================


	/**
	 * Effectuer une requête vers l'API
	 *
	 * Tente d'intercepter les erreurs renvoyées par l'API, cependant, les retours ne sont pas très consistants.
	 * Il faut parfois vérifier derrière les données renvoyées.
	 *
	 * @note
	 * Dans les logs on tronque volontairement le texte pour ne pas les faire exploser
	 *
	 * @param string $endpoint
	 *     Voir endpoints possibles dans la constante ENDPOINTS
	 * @param string $method
	 *     POST | GET
	 * @param array $data
	 *     Tableau associatif des données à envoyer
	 * @return array
	 *     Tableau associatif :
	 *     - dans tous les cas : clé `data` avec le retour de l'API, vide en cas d'erreur
	 *     - si erreur : clé supplémentaire `error` avec un message d'erreur
	 */
	protected function request(string $endpoint, array $data, string $method = 'POST') : array
	{

		include_spip('inc/distant');
		include_spip('inc/utils'); // spip_version_compare

		$response = [];
		$error = '';

		if (!isset($this::ENDPOINTS[$endpoint])) {
			$error = "Requête : enpoint $endpoint inconnu";
		} elseif (!$this->checkCredentials()) {
			$error = 'Requête : données manquantes pour l’authentification';
		} else {

			// Nb : il y a un bug bloquant avec recuperer_url() pour Spip 3.2 : parfois ça force un POST même si on demande un GET.
			// On utilise donc une copie de la bonne version en fallback
			$fn_recuperer_url = (spip_version_compare($GLOBALS['spip_version_branche'], '4.0.0', '>=') ? 'recuperer_url' : 'playht_recuperer_url');

			$request_url = $this::BASE_URL . $this::ENDPOINTS[$endpoint];
			$request_headers = [
				'Authorization' => $this->authorization,
				'X-User-ID'     => $this->userId,
			];
			$request_data = '';
			switch ($method) {
				case 'POST':
					$request_headers = ['Content-Type' => 'application/json'] + $request_headers;
					$request_data    = json_encode($data);
					break;
				case 'GET':
					foreach ($data as $key => $val) {
						$request_url = parametre_url($request_url, $key, $val, '&');
					}
					break;
			}
			$request_options = [
				'methode' => $method,
				'headers' => $request_headers,
				'datas'   => $request_data,
			];
			$request_options = array_filter($request_options);

			// Log de l'envoi
			// On tronque les paragraphes de texte à 80 charactères pour ne pas exploser les logs
			$log_request = $data;
			if (isset($log_request['ssml']) and is_array($log_request['ssml'])) {
				foreach (array_chunk($log_request['ssml'], 2)[0] as $k => $content) {
					$log_request['ssml'][$k] = substr($content, 0, 80);
				}
			}
			$log_request = json_encode($log_request, JSON_PRETTY_PRINT);
			spip_log("$endpoint / requête : $log_request", 'playht_feedback' . LOG_INFO);

			// Go !
			$request = $fn_recuperer_url($request_url, $request_options);
			$response_page = ($request['page'] ?? '');
			$response_json = json_decode($response_page, true);
			$status = ($request['status'] ?? '?');

			// Identifier les erreurs
			// 1) Pas de réponse
			if ($request === false) {
				$error = "Requête sans réponse : $request_url";
			// 2) Retour non JSON
			} elseif ($response_json === null) {
				$error = 'Retour JSON non décodable' . ($response_page ? " : $response_page" : '');
			// 3) Retour JSON comportant une indication d'erreur
			// Les retours d'erreurs ne sont pas consistants et la doc est pas à jour : le combo gagnant ಠ_ಠ
			} elseif (
				$errors_keys = ['errorMessage', 'message', 'error', 'body']
				and $error_key = array_shift(array_intersect($errors_keys, array_keys($response_json)))
			) {
				// Des fois on a une clé `body` qui contient du json ayant lui-même une clé `error`... ¯\_(ツ)_/¯
				if ($error_key === 'body') {
					$body = json_decode($response_json['body'], true);
					$error = ($body['error'] ?? print_r($body, true));
				} else {
					$error = $response_json[$error_key];
				}
			}

			// Log du retour
			$log_response = ($response_page ? json_encode(json_decode($response_page), JSON_PRETTY_PRINT) : $error);
			spip_log("[request $endpoint] statut : $status, réponse : $log_response", 'playht_feedback' . LOG_INFO);
		}

		if ($error) {
			$response['error'] = $error;
			spip_log("[request] $error", 'playht' . _LOG_ERREUR);
		}

		// On renvoie toujours le retour de la requête, même en cas d'erreur
		$response['data'] = (is_array($response_json) ? $response_json : []);

		return $response;
	}

	/**
	 * Préparer un contenu associé à une URL canonique avant sa conversion audio
	 *
	 * Cela crée ou met à jour un « article » chez Play.ht.
	 *
	 * @param array $data
	 *    Données transmises à l'API (pas besoin de donner les choses pour s'authentifier : userId, etc.)
	 *    - **ssml (array)            : tableau des paragraphes à vocaliser
	 *    - **url (string)            : URL canonique du contenu
	 *    - title (string)            : titre de l'audio pour le dashboard Play.ht
	 *    - published_date (string)   : date au format timestamp en millisecondes
	 *    - published_status (string) : draft | '' (?)
	 *    - author_id (string)        : (?)
	 *    - author_name (string)      : Nom de l'auteur du contenu (quid si plusieurs ?)
	 *    - author_image (string)     : url vers image (?)
	 *    - image (bool)              : (?)
	 *    - (?) tags
	 *    - voice
	 *    - narrationStyle
	 *    - globalSpeed
	 *    - readAlongEnabled
	 * @return array
	 *    - Si ok     : tableau correspondant à la clé `article` du JSON retourné
	 *    - Si erreur : tableau vide + envoi d'une Exception.
	 */
	public function requestSetArticleInfo(array $data) : array
	{
		$response = [];
		$error = '';

		// Compléter avec les données d'authentification et les valeurs par défaut obligatoires ou non présentes dans le form de vocalisation
		// Retirer les clés inconnues (TODO : faire une liste stricte des clés possibles ?)
		$data_default = [
			'appId'           => $this->appId,
			'userId'          => $this->userId,
			'voice'           => $this->defaults['voice'],
			// 'narrationStyle'  => $this->defaults['narrationStyle'],
			// 'globalSpeed'     => $this->defaults['globalSpeed'],
			// 'pronunciations'  => $this->defaults['pronunciations'],
			// 'readAlongEnabled' => $this->defaults['readAlongEnabled'],
		];
		$data_remove = ['template' => ''];
		$data = array_diff_key($data, $data_remove);
		$data = array_merge(array_filter($data_default), array_filter($data));
		$request = $this->request('setArticleInfo', $data, 'POST');

		// À ce stade les infos retournées sont optionnelles, on ne vérifie pas la présence de sous-clés spécifiques
		if (isset($request['data']['article'])) {
			$response = $request['data']['article'];
		} else {
			$error = ($request['error'] ?? 'Erreur inconnue (clé `article` absente');
			throw new \Exception($error);
			spip_log("[setArticleInfo] $error", 'playht' . _LOG_ERREUR);
		}

		return $response;
	}

	/**
	 * Lancer la conversion audio d'un contenu associé à une URL canonique
	 *
	 * Retourne l'identifiant de transcription.
	 *
	 * @param array $data
	 *    Données transmises à l'API (pas besoin de donner les choses pour s'authentifier : userId, etc.)
	 *    TODO : décrire le tableau
	 * @return string
	 *     - si ok     : `transcriptionId`
	 *     - Si erreur : chaîne vide + envoi d'une Exception.
	 * */
	public function requestTranscripe(array $data) : string
	{
		$transcription_id = '';
		$error = '';

		// Compléter avec les données d'authentification et les valeurs par défaut
		// Retirer les clés inconnues (TODO : faire une liste stricte des clés possibles ?)
		$data_default = [
			'appId'           => $this->appId,
			'userId'          => $this->userId,
			'voice'           => $this->defaults['voice'],
			// 'narrationStyle'  => $this->defaults['narrationStyle'],
			// 'globalSpeed'     => $this->defaults['globalSpeed'],
			// 'pronunciations'  => $this->defaults['pronunciations'],
			// 'readAlongEnabled' => $this->defaults['readAlongEnabled'],
		];
		$data_remove = ['template' => ''];
		$data = array_diff_key($data, $data_remove);
		$data = array_merge(array_filter($data_default), array_filter($data));
		$request = $this->request('transcripe', $data, 'POST');

		// Il faut la clé transcriptionId dans le retour, sinon on a loupé notre vie
		$transcription_id = ($request['data']['transcriptionId'] ?? '');

		if (!$transcription_id) {
			$error = ($request['error'] ?? 'Erreur inconnue (clé `transcriptionId` absente)');
			throw new \Exception($error);
			spip_log("[transcripe] $error", 'playht' . _LOG_ERREUR);
		}

		return $transcription_id;
	}

	/**
	 * Vérifier le statut d'une conversion audio
	 *
	 * Quand la conversion est finie, retourne 2 infos importantes : l'URL distante et la durée de l'audio.
	 *
	 * @param string $transcription_id
	 * @return array
	 *     Tableau associatif avec indication de l'état de la conversion :
	 *     - conversion en cours  : clé `converted` = 0
	 *     - conversion terminée  : clé `converted` = 1 + `audioUrl`, `audioDuration` et `voice`
	 *     - conversion en erreur : clé `converted` = 2 + envoi d'une Exception
	 *     - conversion inconnue  : clé `converted` = 3 + envoi d'une Exception
	 */
	public function requestArticleStatus(string $transcription_id) : array
	{
		$response = [];
		$error = '';

		$data = ['transcriptionId' => $transcription_id];
		$request = $this->request('articleStatus', $data, 'GET');
		$converted = ($request['data']['converted'] ?? null);
		$request_error = ($request['data']['error'] ?? null);

		// Conversion terminée
		if ($converted === true) {
			$response['converted'] = 1;

		// Conversion en cours
		} elseif ($converted === false and $request_error !== true) {
			$response['converted'] = 0;

		// Conversion explicitement en erreur
		} elseif ($converted === false and $request_error === true) {
			$response['converted'] = 2;
			$error = ($request['data']['errorMessage'] ?? 'Conversion en erreur, raison non indiquée (absence de errorMessage).');

		// Sans réponse
		} else {
			$response['converted'] = 3;
			$error = ($request['error'] ?? 'Erreur inconnue');
		}

		// On renvoie tout, mais avec nos valeurs pour la clé `converted`
		$response = array_merge($request['data'], $response);

		if ($error) {
			// throw new \Exception($error);
			$response['error'] = $error;
			spip_log("[articleStatus] $error", 'playht' . _LOG_ERREUR);
		}

		return $response;
	}

	/**
	 * Définit des prononciations spécifiques pour certains termes
	 *
	 * @param array $pronunciations
	 *     Tableau associatif [terme => prononciation]
	 * @return bool
	 *     si ok : true
	 *     si erreur : false + envoi d'une exception
	 */
	public function requestUserPronunciations(array $pronunciations) : bool
	{
		$response = true;
		$data = [
			'appId'          => $this->appId,
			'userId'         => $this->userId,
			'pronunciations' => $pronunciations,
		];

		$request = $this->request('user-pronunciations', $data);
		$error = ($request['data']['error'] ?? null);

		if ($error) {
			throw new \Exception($error);
			$response = false;
			spip_log("[user-pronunciations] $error", 'playht' . _LOG_ERREUR);
		}

		return $response;
	}

	/**
	 * Lancer la conversion audio d'un texte lambda
	 *
	 * Nb : le texte converti de cette façon est "orphelin", aucune URL ne lui est associée.
	 * Par la suite, on ne peut le modifier que via le dashboard Play.ht
	 * C'est un peu la même chose que la combinaison de setArticleInfo() + transcripe(), mais en moins complet.
	 *
	 * @param array $data
	 *    Données transmises à l'API
	 *     - title (string) : titre de l'audio pour le dashboard Play.ht (optionnel)
	 *     - content (array) : tableau des paragraphes à vocaliser
	 *     - voice
	 *     - narrationStyle
	 *     - globalSpeed
	 *     - readAlongEnabled
	 * @return string
	 *    - Si ok, numéro de la conversion audio donné dans le retour JSON : `transcriptionId`
	 *    - Si erreur, chaîne vide avec envoi d'une Exception.
	 */
	public function requestConvert(array $data) : string
	{

		$transcription_id = '';
		$error = '';

		$data_default = [
			'appId'           => $this->appId,
			'voice'           => $this->defaults['voice'],
			'narrationStyle'  => $this->defaults['narrationStyle'],
			'globalSpeed'     => $this->defaults['globalSpeed'],
			// 'pronunciations'  => $this->defaults['pronunciations'],
			// 'readAlongEnabled' => $this->defaults['readAlongEnabled'],
		];
		$data = array_merge(array_filter($data_default), array_filter($data));
		$response = $this->request('convert', $data, 'POST');
		if (isset($response['transcriptionId'])) {
			$transcription_id = $response['transcriptionId'];
		} else {
			$error = ($response['error'] ?? 'convert : Erreur inconnue');
		}

		if ($error) {
			throw new \Exception($error);
			spip_log("[convert] $error", 'playht' . _LOG_ERREUR);
		}

		return $transcription_id;
	}


	// ========================
	// Manipulation objets Spip
	// ========================


	/**
	 * Lancer la conversion du texte d'un objet Spip
	 *
	 * On passe dans 2 pipelines qui permettent de modifier l'ordre des champs, et le texte final à convertir en audio.
	 * Nb : c'est à l'appelant de créer l'objet `vocalisation` par la suite si besoin.
	 *
	 * @param string $objet
	 *     Type d'objet
	 * @param integer $id_objet
	 *     Numéro de l'objet
	 * @param array $options
	 *     Options, dont celles transmises à l'API
	 *     - voice
	 *     - narrationStyle
	 *     - globalSpeed
	 *     - readAlongEnabled
	 *     - template
	 * @return array
	 *     - Si ok, clé `set` permettant de créer l'objet avec le statut en cours de conversion
	 *     - Si erreur, clé `error` avec le message d'erreur
	 */
	public function convertObject(string $objet, int $id_objet, array $options = []) : array
	{

		include_spip('inc/abstract_sql');
		include_spip('inc/filtres');
		include_spip('base/objets');

		$conversion = [];
		$error = '';
		$set = [];

		try {

			$options_text = ['template' => ($options['template'] ?? null)];
			if ($text_object = $this->getTextObject($objet, $id_objet, $options_text)) {

				// Infos de l'objet.
				$data_object = [];
				// Date au format Unix Epoch en millisecondes : on met celle de la génération de l'audio
				$datetime = new \DateTime();
				$data_object['published_date'] = $datetime->getTimestamp() * 1000;
				// Statut : on met toujours le défaut (publié), pas besoin de drafts
				// $data_object['published_status'] = (objet_test_si_publie($objet, $id_objet) ? '' : 'draft');
				$data_object['published_status'] = '';
				// URL canonique
				$url = $this->getUrlObject($objet, $id_objet);
				$data_object['url'] = $url;
				// Auteurs
				if ($auteurs = sql_allfetsel('A.nom', 'spip_auteurs A INNER JOIN spip_auteurs_liens L On L.id_auteur=A.id_auteur', ['L.objet='.sql_quote($objet), "L.id_objet=$id_objet"])) {
					$noms_auteurs = join(', ', array_column($auteurs, 'nom'));
					$data_object['author_name'] = $noms_auteurs;
				}
				// Empreinte texte
				$hash = $this->hashText($text_object);
				// Langue
				$lang = $this->getLangObject($objet, $id_objet);

				// Data
				// Étape 1 : setArticleInfo
				$data_set_article = array_merge(
					$options,
					$text_object,
					$data_object
				);
				// Étape 2 : transcripe
				$data_transcribe = array_merge(
					$options,
					$text_object,
					['url' => $url]
				);

				// Go !
				if (
					$article = $this->requestSetArticleInfo($data_set_article)
					and $transcription_id = $this->requestTranscripe($data_transcribe)
				) {
					// L'audio est en cours de création : on crée ou met à jour l'objet Spip
					// Son URL sera renseignée ultérieurement via une requête pour vérifier le statut de la conversion
					$set = [
						'objet'             => $objet,
						'id_objet'          => $id_objet,
						'titre'             => ($article['title'] ?? ''),
						'playht_url'        => ($article['url'] ?? ''),
						'playht_id'         => ($article['id'] ?? ''),
						'transcription_id'  => $transcription_id,
						'voice'             => ($article['voice'] ?? ''),
						'narration_style'   => ($article['narrationStyle'] ?? ''),
						'global_speed'      => ($article['globalSpeed'] ?? ''),
						// 'read_along_enabled' => ($article['readAlongEnabled'] ?? ''),
						'hash'              => $hash,
						'sync'              => 1,
						'date'              => $datetime->format('Y-m-d H:i:s'),
						'statut'            => 'conversion',
						'lang'              => $lang,
						'metas'             => serialize($article),
					];
				}
			}

		} catch (\Exception $e) {
			$error = $e->getMessage();
		}

		if ($set) {
			$conversion['set'] = $set;
		} else {
			$error = ($error ?? 'Erreur inconnue');
			$conversion['error'] = $error;
			spip_log("[convertObject] $error", 'playht' . _LOG_ERREUR);
		}

		return $conversion;
	}

	/**
	 * Récupérer le contenu texte d'un objet Spip
	 *
	 * On passe par plusieurs pipelines :
	 * - playht_texte_objet_champs : pour changer l'ordre des champs, insérer du contenu à un endroit précis, etc.
	 * - playht_texte_objet_convert : pour changer le texte final
	 *
	 * @pipeline playht_texte_objet_champs
	 * @pipeline playht_texte_objet_final
	 *
	 * @param string $objet
	 *     Type d'objet
	 * @param int $id_objet
	 *     Numéro de l'objet
	 * @param array $options
	 *     Options éventuelles :
	 *     - template
	 * @return array
	 *     - Si ok, tableau associatif avec 2 clés :
	 *       - `title` (string) : titre utilisé dans le dashboard
	 *       - `ssml`  (array)  : tableau de tous les paragraphes de texte, en commençant par le titre.
	 *     - Si erreur, tableau vide + Exception
	 */
	public function getTextObject(string $objet, int $id_objet, array $options = []) : array
	{

		include_spip('base/objets');

		$text = [];
		$table_objet = table_objet_sql($objet);
		$cle_objet = id_table_objet($objet);

		// Vérifier l'objet, des fois que...
		if (!sql_countsel($table_objet, "$cle_objet=$id_objet")) {
			throw new \Exception("Objet $objet $id_objet inexistant");

		} else {

			// On récupère le texte HTML classé par champs, le titre en 1er
			// Puis passage dans un pipeline : changer l'ordre des champs, ajouter du contenu à un endroit précis, etc.
			$textFields = $this->getTextObjectFields($objet, $id_objet);
			$textFields = pipeline(
				'playht_texte_objet_champs',
				[
					'args' => [
						'objet'    => $objet,
						'id_objet' => $id_objet,
					],
					'data' => $textFields,
				]
			);

			// Title = titre utilisé dans le dashboard (string)
			// Formater en texte brut + séparation des paragraphes
			$title = implode(' ', $this->formatText([$textFields['titre']]));
			$text['title'] = "[$objet $id_objet] $title"; // Titre utilisé dans le dashboard (string)

			// ssml = contenu converti en audio (array)
			// Appliquer le template + formater en texte brut + séparation des paragraphes
			if ($template = ($options['template'] ?? null)) {
				$textFields = $this->applyTemplate($textFields, $template, $objet, $id_objet);
			}
			$textFormated = $this->formatText($textFields);
			$text['ssml'] = $this->text_to_ssml($textFormated);

			// Passage dans un pipeline pour le texte final
			$text = pipeline(
				'playht_texte_objet_final',
				[
					'args' => [
						'objet'    => $objet,
						'id_objet' => $id_objet,
					],
					'data' => $text,
				]
			);

			// Dernière vérif
			if (!$text = array_filter($text)) {
				throw new \Exception("Objet $objet $id_objet : aucun contenu texte trouvé");
			}
		}

		return $text;
	}

	/**
	 * Renvoie le texte HTML d'un objet pour chaque champ configuré
	 *
	 * TODO :
	 * - Option pour les notes de base de page
	 * - Surtitre / soustitre : what to do ?
	 *
	 * @param string $objet
	 * @param integer $id_objet
	 * @return array
	 *     - si ok, tableau associatif de paires champ => texte HTML, avec toujours le titre en 1er (même si vide)
	 *     - si erreur, tableau vide
	 */
	protected function getTextObjectFields(string $objet, int $id_objet) : array
	{

		include_spip('inc/filtres');
		include_spip('inc/texte');
		include_spip('public/composer'); // calculer_notes()

		$text = [];

		// D'abord le titre
		$text['titre'] = generer_info_entite($id_objet, $objet, 'titre');

		// Puis tous les autres champs texte
		// S'ils ne sont pas déclarés pour l'objet, on prend une liste générique
		if (!$champs_vocalisation = objet_info($objet, 'champs_vocalisation')) {
			$champs_defaut = [
				'chapo',
				'descriptif',
				'texte',
				'ps'
			];
			$champs_objets = array_keys(objet_info($objet, 'field'));
			$champs_vocalisation = array_intersect($champs_defaut, $champs_objets);
		}
		foreach ($champs_vocalisation as $champ) {
			$text[$champ] = generer_info_entite($id_objet, $objet, $champ); // Passe d'office par propre
		}

		// Notes de bas de page
		if ($notes = calculer_notes()) {
			$text['notes'] = $notes;
		}

		return $text;
	}

	/**
	 * Préparer le contenu texte en vue de sa conversion en audio
	 *
	 * On passe en texte brut :
	 * - retrait des balises et entités html.
	 * - on sépare en blocs (paragraphes, listes, etc.).
	 *
	 * @param array $text
	 *     Tableau de portions de textes HTML, classés à priori par champs, le titre en 1er
	 * @return array
	 *     Tableau de blocs de texte brut
	 */
	protected function formatText(array $text) : array
	{
		include_spip('inc/filtres');

		$text_portions = [];
		foreach($text as $text_bloc) {

			// On sépare les blocs (paragraphes, listes, etc.) par N sauts de lignes pour les distinguer des simples retours lignes
			$divider_nb = 2;
			$divider = str_repeat("\n", $divider_nb);
			$text_bloc = preg_replace('/<(p|'._BALISES_BLOCS.')/', "$divider<$1", $text_bloc);

			// On passe en texte brut et on remplace certains caractères funkys
			$text_bloc = trim(supprimer_tags(html_entity_decode($text_bloc)));
			$text_bloc = str_replace("\xc2\xa0", ' ', $text_bloc);

			// Limiter à N sauts de ligne max
			$text_bloc = preg_replace('/(?:(?:\r\n|\r|\n)\s*){'.$divider_nb.'}/s', $divider, $text_bloc);

			// Séparer en blocs : N sauts de ligne = 1 bloc
			$text_bloc_portions = explode($divider, $text_bloc);
			// Dans chaque bloc, plus besoin de saut de ligne : on les retire
			foreach ($text_bloc_portions as $k => $portion) {
				$text_bloc_portions[$k] = preg_replace('/(?:(?:\r\n|\r|\n)\s*){1}/s', ' ', $portion);
			}
			$text_portions = array_merge($text_portions, $text_bloc_portions);
			$text_portions = array_filter($text_portions);
		}

		return $text_portions;
	}

	/**
	 * Convertit un tableau de paragraphes de textes au format ssml
	 *
	 * Minimaliste pour l'instant : on encapsule dans des balises `<speak>` + `<p>`
	 *
	 * @param array $text
	 *     Tableau de paragraphes de texte
	 * @return array
	 */
	protected function text_to_ssml(array $text) : array
	{
		$ssml = [];
		foreach ($text as $paragraph) {
			$ssml[] = "<speak><p>$paragraph</p></speak>";
		}
		return $ssml;
	}

	/**
	 * Appliquer un template
	 *
	 * @param array $text
	 *     Tableau associatif de paires champ => texte HTML, avec le titre en 1er
	 * @param string $template
	 *     Identifiant du template
	 * @param string $objet
	 *     Type d'objet
	 * @param integer $id_objet
	 *     Identifiant de l'objet
	 * @return array
	 *     Tableau avec le texte modifié
	 */
	protected function applyTemplate(array $text, string $template, string $objet, int $id_objet) : array
	{

		include_spip('inc/filtres');
		include_spip('inc/filtres_dates');

		if (
			$template_text = ($this->config['templates'][$template]['template'] ?? null)
			and $lang = $this->getLangObject($objet, $id_objet)
			and $template_text = extraire_multi($template_text, $lang)
			and $regex_token = '/(@[^@]+@)/'
			and preg_match($regex_token, $template_text, $token_all)
		) {

			// On extrait le titre et le reste du texte, puis on met tout à plat
			$divider = "\n\n";
			$text_title = $text['titre'];
			$text_content = implode($divider, array_splice($text, 1));
			$text_flat = "$text_title $divider $text_content";

			// On prépare les substitutions des tokens
			preg_match_all($regex_token, $template_text, $tokens_all);
			$tokens = [];
			foreach ($tokens_all[0] as $token) {
				switch ($token) {
					case '@titre@':
						$tokens[$token] = $text_title;
						break;
					case '@contenu@':
						$tokens[$token] = $text_content;
						break;
					case '@date@':
						$date = generer_info_entite($id_objet, $objet, 'date');
						$tokens[$token] = affdate($date);
						break;
					case '@auteurs@':
						$substitut = '';
						if ($auteurs = sql_allfetsel('A.nom', 'spip_auteurs A INNER JOIN spip_auteurs_liens L ON A.id_auteur=L.id_auteur', ['objet='.sql_quote($objet), "id_objet=$id_objet"])) {
							$auteurs = array_column($auteurs, 'nom');
							$substitut = implode(', ', $auteurs);
						}
						$tokens[$token] = $substitut;
						break;
					case '@rubrique@':
						$substitut = '';
						if ($id_rubrique = generer_info_entite($id_objet, $objet, 'id_rubrique', '*')) {
							$substitut = generer_info_entite($id_rubrique, 'rubrique', 'titre');
						}
						$tokens[$token] = $substitut;
						break;
					case '@site_nom@':
						$tokens[$token] = extraire_multi($GLOBALS['meta']['nom_site'], $lang);
						break;
					case '@site_slogan@':
						$tokens[$token] = extraire_multi($GLOBALS['meta']['slogan_site'], $lang);
						break;
				}
			}
			$tokens = pipeline('playht_tokens_objet', [
				'args' => ['objet' => $objet, 'id_objet' => $id_objet],
				'data' => $tokens,
			]);

			// On procède au grand remplacement civilisationnel
			// On s'assure que @contenu@ soit séparé sur une ligne
			$template_text = str_replace('@contenu@', "$divider@contenu@$divider", $template_text);
			$search = array_unique($tokens_all[0]);
			$replace = array_values(array_intersect_key($tokens, array_flip($search)));
			$text = str_replace($search, $replace, $template_text);
			$text = [$text]; // Hmmm, à revoir :p
		}

		return $text;
	}

	/**
	 * Récupérer l'URL canonique d'un objet Spip
	 *
	 * Il s'agit de l'URL transmise à Play.ht, elle fait office d'identifiant unique.
	 *
	 * - On utilise l'URL raccourcie invariante : http://lesite.ltd/<objet><id_objet>
	 * - Comme base, on utilise celle configurée dans le plugin si présente
	 *
	 * @param string $objet
	 * @param integer $id_objet
	 * @return string
	 */
	public function getUrlObject(string $objet, int $id_objet) : string
	{
		$url = '';
		$url_config = ($this->config['base_url'] ?? null);
		$url_base = ($url_config ?: $GLOBALS['meta']['adresse_site']);
		$url_base = rtrim($url_base, '/');
		$url = "$url_base/$objet$id_objet";
		/*
		include_spip('inc/urls');
		$url = generer_url_entite_absolue($id_objet, $objet);
		if ($base_url_playht = $this->config['base_url']) {
			$base_url_playht = rtrim($base_url_playht, '/');
			$base_url_spip = $GLOBALS['meta']['adresse_site'];
			$url = str_replace($base_url_spip, $base_url_playht, $url);
		}
		*/
		return $url;
	}

	/**
	 * Récupérer la langue d'un objet
	 *
	 * @param string $objet
	 * @param integer $id_objet
	 * @return string
	 */
	public function getLangObject(string $objet, int $id_objet) : string
	{
		include_spip('inc/filtres');
		$lang = (generer_info_entite($id_objet, $objet, 'lang', '*') ?: $GLOBALS['spip_lang']);
		return $lang;
	}

	/**
	 * Vérifie si l'empreinte du texte d'un objet Spip est raccord avec son texte
	 *
	 * @param string $objet
	 * @param integer $id_objet
	 * @return boolean
	 */
	public function checkHashObject(string $objet, int $id_objet) : bool
	{
		include_spip('base/abstract_sql');
		$sync = false;

		if ($hash = sql_getfetsel('hash', 'spip_vocalisations', ['objet='.sql_quote($objet), "id_objet=$id_objet"])) {
			$text_object = $this->getTextObject($objet, $id_objet);
			$hash_text = $this->hashText($text_object);
			$sync = ($hash === $hash_text);
		}

		return $sync;
	}


	// ======
	// Divers
	// ======


	/**
	 * Retourne l'empreinte md5 d'un texte
	 *
	 * @param array $text
	 *     Tableau associatif : `title` (string) + `ssml` (array)
	 * @return string
	 */
	private function hashText(array $text) : string
	{
		$raw_text = $text['title'] ?? '';
		foreach ($text['ssml'] as $ssml) {
			$raw_text .= $ssml;
		}
		$hash = md5($raw_text);
		return $hash;
	}

	/**
	 * Vérifier qu'on a ce qu'il faut pour utiliser l'API
	 *
	 * @return bool
	 */
	private function checkCredentials()
	{
		$credentials = false;
		if (
			strlen($this->authorization)
			and strlen($this->userId)
			and strlen($this->appId)
		) {
			$credentials = true;
		}
		return $credentials;
	}


	// ======
	// Listes
	// ======


	/**
	 * Lister les voix
	 *
	 * @param string|null $lang
	 *     Code langue Spip pour limiter aux voix correspondantes
	 * @return array
	 *     Tableau associatif : Nom langue Playht => [Identifiant voix => Nom de la voix]
	 */
	public static function listVoices(? string $lang_spip = null) : array
	{
		include_spip('inc/PlayHtRepository');
		$voices       = [];
		$repo         = new Repository();
		$voices_repo  = $repo::getVoices();
		$langs_spip   = $repo::getLangsSpip();
		$langs_playht = self::listLangs();

		// Refaire un tableau classé par langue
		foreach ($voices_repo as $voice) {
			$voice_lang                 = $voice['lang'];
			$voice_lang_name            = ($langs_playht[$voice_lang] ?? $voice_lang);
			$voice_id                   = $voice['value'];
			$voice_name                 = $voice['name'];
			$voices[$voice_lang_name]   = array_merge($voices[$voice_lang_name] ?? [], [$voice_id => $voice_name]);
		}

		// Limiter aux voix correspondantes à un code langue spip si demandé
		if ($langs = ($langs_spip[$lang_spip] ?? null)) {
			$langs_names = [];
			foreach ($langs as $lang) {
				$langs_names[] = ($langs_playht[$lang] ?? $lang);
			}
			$voices = array_intersect_key($voices, array_flip($langs_names));
		}

		return $voices;
	}

	/**
	 * Lister les langues
	 *
	 * @param string|null $lang_playht
	 *     Identifiant langue Playht pour retourner le nom de cette langue en particulier
	 * @return array|string
	 *     - Soit tableau associatif : identifiant langue Playht => Nom de la langue
	 *     - Soit le nom d'une langue si elle existe
	 */
	public static function listLangs(? string $lang_playht = null)
	{
		include_spip('inc/PlayHtRepository');
		$repo = new Repository();
		$langs_repo = $repo::getLangs();
		$langs = [];
		foreach ($langs_repo as $k => $description) {
			$lang_id = $description['value'];
			$langs[$lang_id] = _T("playht-repo:lang_${lang_id}");
		}
		$return = ($langs[$lang_playht] ?? $langs);

		return $return;
	}

	/**
	 * Lister les styles de narration
	 *
	 * @param string|null $narration_style
	 *     Pour renvoyer le nom d'un style en particulier
	 * @return array|string
	 *     - Soit un tableau associatif style => nom
	 *     - Soit le nom d'un style si il existe
	 */
	public static function listNarrationStyles(? string $narration_style = null)
	{
		include_spip('inc/PlayHtRepository');
		$repo = new Repository();
		$styles = [];
		foreach ($repo::getNarrationStyles() as $style) {
			$styles[$style] = _T("playht-repo:narration_style_$style");
		}
		$return = ($styles[$narration_style] ?? $styles);

		return $return;
	}

	/**
	 * Liste les tokens pour les templates
	 *
	 * @pipeline playht_lister_tokens
	 *
	 * @return array
	 */
	public static function listTemplatesTokens() : array
	{
		$tokens = [
			'@titre@'       => _T('playht:token_titre'),
			'@contenu@'     => _T('playht:token_contenu'),
			'@date@'        => _T('playht:token_date'),
			'@auteurs@'     => _T('playht:token_auteurs'),
			'@rubrique@'    => _T('playht:token_rubrique'),
			'@site_nom@'    => _T('playht:token_site_nom'),
			'@site_slogan@' => _T('playht:token_site_slogan'),
		];
		$tokens = pipeline('playht_lister_tokens', $tokens);

		return $tokens;
	}

	// =======
	// Setters
	// =======

	/**
	 * Set la config
	 *
	 * @return void
	 */
	private function setConfig() : void
	{
		include_spip('inc/config');
		$this->config = lire_config('playht');
	}

	/**
	 * Set les identifiants nécessaires à l'utilisation de l'API
	 *
	 * @return void
	 */
	private function setCredentials() : void
	{
		$this->userId        = ($this->config['userid'] ?? null);
		$this->authorization = ($this->config['authorization'] ?? null);
		$this->appId         = ($this->config['appid'] ?? null);
	}

	/**
	 * Set les valeurs par défaut
	 *
	 * On ne met que les valeurs obligatoires et celles non présentes dans le formulaire de vocalisation.
	 *
	 * @return void
	 */
	private function setDefaults() : void
	{

		// Voix par défaut si aucune configurée = 1ère voix trouvée pour la langue du site
		$voice_default = null;
		if (empty($this->config['voice'])) {
			$lang_spip = $GLOBALS['spip_lang'];
			$voices = $this->listVoices($lang_spip);
			$voices_lang = array_shift($voices);

			$voice_default = array_flip($voices_lang);
			$voice_default = array_shift($voice_default);
		}

		// Set
		$this->defaults = [
			'voice'          => ($this->config['voice'] ?? $voice_default),
			// 'pronunciations' => ($this->config['pronunciations'] ?? []),
		];
	}

}
