<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_vocalisation' => 'Add this vocalization',

	// C
	'champ_audio_duration_label' => 'Duration',
	'champ_audio_url_label' => 'Audio URL',
	'champ_global_speed_label' => 'Global speed',
	'champ_global_speed_explication' => 'Number between 20 and 200, can be left blank.',
	'champ_id_objet_label' => 'Number of the linked content',
	'champ_objet_label' => 'Type of linked content',
	'champ_objet' => 'Linked content',
	'champ_narration_style_label' => 'Narration style',
	'champ_transcription_id_label' => 'Transcript ID',
	'champ_article_id_label' => 'Article ID',
	'champ_titre_label' => 'Title',
	'champ_voice_label' => 'Voice',
	'champ_template_label' => 'Template',
	'champ_texte_apercu_label' => 'Text preview',
	'confirmer_supprimer_vocalisation' => 'Do you confirm the deletion of this vocalization?',

	// I
	'icone_creer_vocalisation' => 'Create a vocalization',
	'icone_modifier_vocalisation' => 'Modify this vocalization',
	'info_1_vocalisation' => 'One vocalization',
	'info_aucun_vocalisation' => 'No vocalization',
	'info_vocalisation_oui' => 'This content is vocalized',
	'info_vocalisation_non' => 'Content not vocalized',
	'info_vocalisation_objet_oui' => '@objet@ vocalized',
	'info_vocalisation_objet_non' => '@objet@ not vocalized',
	'info_nb_vocalisations' => '@nb@ vocalizations',
	'info_vocalisations_auteur' => 'Vocalizations of this author',
	'info_conversion' => 'Conversion in progress.',
	'info_audio_desynchro' => 'Audio desynchronized',
	'info_audio_desynchro_explication' => 'The text has been modified since the audio was generated',

	// R
	'retirer_lien_vocalisation' => 'Remove this vocalization',
	'retirer_tous_liens_vocalisations' => 'Remove all vocalizations',

	// S
	'supprimer_vocalisation' => 'Delete this vocalization',

	// T
	'texte_ajouter_vocalisation' => 'Add a vocalization',
	'texte_changer_statut_vocalisation' => 'This vocalization is:',
	'texte_creer_associer_vocalisation' => 'Create and associate a vocalization',
	'texte_definir_comme_traduction_vocalisation' => 'This vocalization is a translation of vocalization number:',
	'texte_statut_erreur' => 'Audio conversion in error',
	'texte_statut_conversion' => 'Audio conversion in progress',
	'texte_statut_publie' => 'Audio conversion completed',
	'titre_langue_vocalisation' => 'Language of this vocalization',
	'titre_logo_vocalisation' => 'Logo of this vocalization',
	'titre_objets_lies_vocalisation' => 'Linked to this vocalization',
	'titre_page_vocalisations' => 'Vocalizations',
	'titre_vocalisation' => 'Vocalization',
	'titre_vocalisations' => 'Vocalizations',
	'titre_vocalisations_rubrique' => 'Vocalizations of the section',
);
