<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'lang_english-us'     => 'English (US)',
	'lang_english-au'     => 'English (AU)',
	'lang_english-uk'     => 'English (UK)',
	'lang_english-indian' => 'English (Indian)',
	'lang_english-welsh'  => 'English (Welsh)',
	'lang_french'         => 'French',
	'lang_french-canada'  => 'French (Canada)',
	'lang_german'         => 'German',
	'lang_dutch'          => 'Dutch',
	'lang_italian'        => 'Italian',
	'lang_japanese'       => 'Japanese',
	'lang_korean'         => 'Korean',
	'lang_portuguese-br'  => 'Portuguese (BR)',
	'lang_portuguese'     => 'Portuguese',
	'lang_spanish'        => 'Spanish',
	'lang_spanish-us'     => 'Spanish (US)',
	'lang_spanish-mx'     => 'Spanish (MX)',
	'lang_swedish'        => 'Swedish',
	'lang_turkish'        => 'Turkish',
	'lang_welsh'          => 'Welsh',
	'lang_danish'         => 'Danish',
	'lang_icelandic'      => 'Icelandic',
	'lang_norwegian'      => 'Norwegian',
	'lang_polish'         => 'Polish',
	'lang_romanian'       => 'Romanian',
	'lang_russian'        => 'Russian',
	'lang_arabic'         => 'Arabic',
	'lang_chinese'        => 'Chinese',
	'lang_hindi'          => 'Hindi',
	'lang_vietnamese'     => 'Vietnamese',
	'lang_filipino'       => 'Filipino',
	'lang_indonesian'     => 'Indonesian',
	'lang_czech'          => 'Czech',
	'lang_greek'          => 'Greek',
	'lang_hungarian'      => 'Hungarian',
	'lang_slovak'         => 'Slovak',
	'lang_ukrainian'      => 'Ukrainian',
	'lang_finnish'        => 'Finnish',

	// N
	'narration_style_regular' => 'Regular',
	'narration_style_news' => 'News',
	'narration_style_conversational' => 'Conversational',

);
