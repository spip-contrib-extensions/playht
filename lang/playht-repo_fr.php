<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'lang_english-us'     => 'Anglais (US)',
	'lang_english-au'     => 'Anglais (AU)',
	'lang_english-uk'     => 'Anglais (UK)',
	'lang_english-indian' => 'Anglais (Indien)',
	'lang_english-welsh'  => 'Anglais (Welsh)',
	'lang_french'         => 'Français',
	'lang_french-canada'  => 'Français (Canada)',
	'lang_german'         => 'Allemand',
	'lang_dutch'          => 'Néerlandais',
	'lang_italian'        => 'Italien',
	'lang_japanese'       => 'Japonais',
	'lang_korean'         => 'Coréen',
	'lang_portuguese-br'  => 'Portuguais (BR)',
	'lang_portuguese'     => 'Portuguais',
	'lang_spanish'        => 'Espagnol',
	'lang_spanish-us'     => 'Espagnol (US)',
	'lang_spanish-mx'     => 'Espagnol (MX)',
	'lang_swedish'        => 'Suédois',
	'lang_turkish'        => 'Turque',
	'lang_welsh'          => 'Gallois',
	'lang_danish'         => 'Danois',
	'lang_icelandic'      => 'Islandais',
	'lang_norwegian'      => 'Norvégien',
	'lang_polish'         => 'Polonais',
	'lang_romanian'       => 'Roumain',
	'lang_russian'        => 'Russe',
	'lang_arabic'         => 'Arabe',
	'lang_chinese'        => 'Chinois',
	'lang_hindi'          => 'Hindou',
	'lang_vietnamese'     => 'Vietnamien',
	'lang_filipino'       => 'Filippin',
	'lang_indonesian'     => 'Indonésien',
	'lang_czech'          => 'Tchèque',
	'lang_greek'          => 'Grèque',
	'lang_hungarian'      => 'Hongrois',
	'lang_slovak'         => 'Slovaque',
	'lang_ukrainian'      => 'Ukrainien',
	'lang_finnish'        => 'Finnois',

	// N
	'narration_style_regular' => 'Normal',
	'narration_style_news' => 'Actualités',
	'narration_style_conversational' => 'Conversation',

);
