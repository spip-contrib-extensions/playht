<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_vocalisation' => 'Ajouter cette vocalisation',

	// C
	'champ_audio_duration_label' => 'Durée',
	'champ_audio_url_label' => 'URL de l’audio',
	'champ_global_speed_label' => 'Vitesse de lecture',
	'champ_global_speed_explication' => 'Nombre entre 20 et 200, peut être laissé vide.',
	'champ_id_objet_label' => 'Numéro du contenu lié',
	'champ_objet_label' => 'Type de contenu lié',
	'champ_objet' => 'Contenu lié',
	'champ_narration_style_label' => 'Style de narration',
	'champ_transcription_id_label' => 'Identifiant de transcription',
	'champ_article_id_label' => 'Identifiant de l’article',
	'champ_titre_label' => 'Titre',
	'champ_voice_label' => 'Voix',
	'champ_template_label' => 'Template',
	'champ_texte_apercu_label' => 'Aperçu du texte',
	'confirmer_supprimer_vocalisation' => 'Confirmez-vous la suppression de cette vocalisation ?',

	// I
	'icone_creer_vocalisation' => 'Créer une vocalisation',
	'icone_modifier_vocalisation' => 'Modifier cette vocalisation',
	'info_1_vocalisation' => 'Une vocalisation',
	'info_aucun_vocalisation' => 'Aucune vocalisation',
	'info_vocalisation_oui' => 'Ce contenu est vocalisé',
	'info_vocalisation_non' => 'Contenu non vocalisé',
	'info_vocalisation_objet_oui' => '@objet@ vocalisé',
	'info_vocalisation_objet_non' => '@objet@ non vocalisé',
	'info_nb_vocalisations' => '@nb@ vocalisations',
	'info_vocalisations_auteur' => 'Les vocalisations de cet auteur',
	'info_conversion' => 'Conversion en cours.',
	'info_audio_desynchro' => 'Audio désynchronisé',
	'info_audio_desynchro_explication' => 'Le texte a été modifié depuis la génération de l’audio',

	// R
	'retirer_lien_vocalisation' => 'Retirer cette vocalisation',
	'retirer_tous_liens_vocalisations' => 'Retirer toutes les vocalisations',

	// S
	'supprimer_vocalisation' => 'Supprimer cette vocalisation',

	// T
	'texte_ajouter_vocalisation' => 'Ajouter une vocalisation',
	'texte_changer_statut_vocalisation' => 'Cette vocalisation est :',
	'texte_creer_associer_vocalisation' => 'Créer et associer une vocalisation',
	'texte_definir_comme_traduction_vocalisation' => 'Cette vocalisation est une traduction de la vocalisation numéro :',
	'texte_statut_erreur' => 'Conversion audio en erreur',
	'texte_statut_conversion' => 'Conversion audio en cours',
	'texte_statut_publie' => 'Conversion audio terminée',
	'titre_langue_vocalisation' => 'Langue de cette vocalisation',
	'titre_logo_vocalisation' => 'Logo de cette vocalisation',
	'titre_objets_lies_vocalisation' => 'Liés à cette vocalisation',
	'titre_page_vocalisations' => 'Les vocalisations',
	'titre_vocalisation' => 'Vocalisation',
	'titre_vocalisations' => 'Vocalisations',
	'titre_vocalisations_rubrique' => 'Vocalisations de la rubrique',
);
