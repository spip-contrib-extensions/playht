<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'playht_titre' => 'Play.ht',

	// B
	'bouton_convertir' => 'Convert to audio',
	'bouton_verifier_conversion' => 'Check conversion progress',
	'bouton_maj' => 'Update audio',
	'bouton_modifier' => 'Edit',
	'bouton_supprimer' => 'Delete',
	'bouton_supprimer_confirm' => 'This will delete the audio locally. Don\'t forget to delete it in the <a href="https://play.ht/app/audio-files">Play.ht dashboard</a> too.',
	'bouton_supprimer_post' => 'The audio has been deleted locally. Don\'t forget to delete it in the <a href="https://play.ht/app/audio-files">Play.ht dashboard</a> too.',
	'bouton_download' => 'Download audio',
	'bouton_dashboard' => 'Play.ht Dashboard',
	'bouton_texte' => 'Text preview',
	'bouton_player' => 'Audio player',

	// M
	'message_ok_transcripting' => 'Audio transcription in progress: the operation may take several minutes.',
	'message_ok_transcripting_check' => 'Check the progress in a few moments.',
	'message_ok_transcripting_finie' => 'Audio transcription finished.',
	'message_ok_transcripting_encours' => 'Audio transcription in progress. Check back in a few moments.',
	'message_ok_template_ajoute' => 'Template « @nom@ » has been added',
	'message_ok_template_supprime' => 'Template « @nom@ » has been deleted',
	'message_erreur_transcripting' => 'Transcription failed: @erreur@',
	'message_erreur_transcripting_inconnu' => 'Unknown error. Check back in a few moments.',
	'message_erreur_creer' => 'Vocalization creation failed',
	'message_erreur_modifier' => 'Vocalization update failed: @erreur@',
	'message_info_modifier_vocalisation' => 'Any change made from the Play.ht dashboard will be overwritten.',
	'message_info_templates' => 'Templates allow you to customize the text of vocalized contents.
		<br>You can use the following tokens.',
		'message_info_dashboard' => 'Some settings can be configured via the Play.ht dashboard',

	// C
	'cfg_appid_label' => 'Application identifier (App-ID)',
	'cfg_bouton_ajouter_template_label' => 'Add a template',
	'cfg_bouton_ajouter_pronunciation_label' => 'Add a line',
	'cfg_bouton_supprimer_template_label' => 'Delete this template',
	'cfg_authorization_label' => 'Secret key (Authorization)',
	'cfg_credentials_label' => 'Play.ht account',
	'cfg_read_along_enabled_label' => 'Read along',
	'cfg_read_along_enabled_label_case' => 'Enable read along',
	'cfg_erreur_langues_notok' => 'No voice for this language',
	'cfg_global_speed_label' => 'Default reading speed',
	'cfg_narration_style_label' => 'Default narration style',
	'cfg_objets_label' => 'Vocalizable contents',
	'cfg_options_label' => 'Options',
	'cfg_objets_explication' => 'Allow vocalization of the following contents',
	'cfg_template_nom_label' => 'Name of the template',
	'cfg_template_texte_label' => 'Text',
	'cfg_template_texte_explication' => 'Use at least @titre@ and @contenu@ tokens, separated by a line.',
	'cfg_templates_label' => 'Templates',
	'cfg_pronunciations_label' => 'Pronunciations',
	'cfg_pronunciations_term' => 'Term',
	'cfg_pronunciations_pronunciation' => 'Pronunciation',
	'cfg_pronunciations_explication' => 'You can specify the pronunciation of certain terms.
		<br>Example: <code>Play.ht → Play dot H T</code>',
	'cfg_titre_parametrages' => 'Settings',
	'cfg_userid_label' => 'User identifier (X-User-ID)',
	'cfg_base_url_label' => 'Alternative base URL',
	'cfg_base_url_explication' => 'On a site under development, you can specify an alternative base URL to use instead of the site URL.
		<br>It will be used for the canonical URLs of vocalized contents.',
	'cfg_voice_label' => 'Default voice',
	'cfg_voice_lang_label' => 'Default voice: @lang@',
	'cfg_onglet_general' => 'General',
	'cfg_onglet_' => 'General',
	'cfg_onglet_templates' => 'Templates',
	'cfg_onglet_pronunciations' => 'Pronunciations',

	// T
	'titre_page_configurer_playht' => 'Configure vocalizations',
	'token_titre' => 'Content title',
	'token_contenu' => 'Content text',
	'token_date' => 'Publication date',
	'token_auteurs' => 'Authors names',
	'token_rubrique' => 'Section name',
	'token_site_nom' => 'Site name',
	'token_site_slogan' => 'Site slogan',
);
