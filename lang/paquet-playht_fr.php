<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'playht_description' => 'Intégration du lecteur audio de Play.ht pour la vocalisation des contenus',
	'playht_nom' => 'Play.ht',
	'playht_slogan' => 'Vocalisation des contenus',
);
