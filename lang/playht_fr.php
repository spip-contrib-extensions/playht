<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'playht_titre' => 'Play.ht',

	// B
	'bouton_convertir' => 'Convertir en audio',
	'bouton_verifier_conversion' => 'Vérifier la conversion',
	'bouton_maj' => 'Mettre à jour l’audio',
	'bouton_modifier' => 'Modifier',
	'bouton_supprimer' => 'Supprimer',
	'bouton_supprimer_confirm' => 'Cela va supprimer l’audio localement. N’oubliez pas de le supprimer également dans <a href="https://play.ht/app/audio-files">le dashboard de Play.ht</a>.',
	'bouton_supprimer_post' => 'L’audio a été supprimé localement. N’oubliez pas de le supprimer également dans <a href="https://play.ht/app/audio-files">le dashboard de Play.ht</a>.',
	'bouton_download' => 'Télécharger l’audio',
	'bouton_dashboard' => 'Dashboard Play.ht',
	'bouton_texte' => 'Aperçu du texte',
	'bouton_player' => 'Lecteur audio',
	'bouton_dashboard' => 'Dashboard Play.ht',

	// M
	'message_ok_transcripting' => 'Conversion audio en cours : l’opération peut prendre plusieurs minutes',
	'message_ok_transcripting_check' => 'Vérifiez l’avancement dans quelques instants.',
	'message_ok_transcripting_finie' => 'Conversion audio terminée.',
	'message_ok_transcripting_encours' => 'Conversion audio en cours. Revérifiez dans quelques instants.',
	'message_ok_template_ajoute' => 'Le template « @nom@ » a bien été ajouté',
	'message_ok_template_supprime' => 'Le template « @nom@ » a été supprimé',
	'message_erreur_transcripting' => 'Conversion en erreur : @erreur@',
	'message_erreur_transcripting_inconnu' => 'Erreur inconnue. Revérifiez dans quelques instants.',
	'message_erreur_creer' => 'La création de la vocalisation a échoué',
	'message_erreur_modifier' => 'La mise à jour de la vocalisation a échoué : @erreur@',
	'message_info_modifier_vocalisation' => 'Tout changement effectué depuis le dashboard Play.ht sera écrasé.',
	'message_info_templates' => 'Les templates permettent de personnaliser les textes vocalisés.
		<br>Vous pouvez utiliser les tokens ci-dessous.',
	'message_info_dashboard' => 'Certaines options sont confiurables dans le dashboard de Play.ht',

	// C
	'cfg_appid_label' => 'Identifiant application (App-ID)',
	'cfg_bouton_ajouter_template_label' => 'Ajouter un template',
	'cfg_bouton_ajouter_pronunciation_label' => 'Ajouter une ligne',
	'cfg_bouton_supprimer_template_label' => 'Supprimer ce template',
	'cfg_authorization_label' => 'Clé secrète (Authorization)',
	'cfg_credentials_label' => 'Compte Play.ht',
	'cfg_read_along_enabled_label' => 'Read along',
	'cfg_read_along_enabled_label_case' => 'Activer la lecture au fil',
	'cfg_erreur_langues_notok' => 'Aucune voix adaptée à cette langue',
	'cfg_global_speed_label' => 'Vitesse de lecture par défaut',
	'cfg_narration_style_label' => 'Style de narration par défaut',
	'cfg_objets_label' => 'Contenus vocalisables',
	'cfg_options_label' => 'Options',
	'cfg_objets_explication' => 'Permettre la vocalisation des contenus suivants',
	'cfg_template_nom_label' => 'Nom du template',
	'cfg_template_texte_label' => 'Texte',
	'cfg_template_texte_explication' => 'Les tokens @titre@ et @contenu@ sont obligatoires, séparés par une ligne.',
	'cfg_templates_label' => 'Templates',
	'cfg_pronunciations_label' => 'Prononciations',
	'cfg_pronunciations_term' => 'Terme',
	'cfg_pronunciations_pronunciation' => 'Prononciation',
	'cfg_pronunciations_explication' => 'Vous pouvez préciser la prononciation de certains termes.
		<br>Exemple : <code>Play.ht → Play point H T</code>',
	'cfg_titre_parametrages' => 'Paramétrages',
	'cfg_userid_label' => 'Identifiant utilisateur (X-User-ID)',
	'cfg_base_url_label' => 'URL de base alternative',
	'cfg_base_url_explication' => 'Sur un site en développement, vous pouvez indiquer une URL de base alternative à utiliser à la place de l’URL du site.
		<br>Elle sera utilisée pour l’URL canonique des contenus vocalisés.',
	'cfg_voice_label' => 'Voix par défaut',
	'cfg_voice_lang_label' => 'Voix par défaut : @lang@',
	'cfg_onglet_general' => 'Généralités',
	'cfg_onglet_' => 'Généralités',
	'cfg_onglet_templates' => 'Templates',
	'cfg_onglet_pronunciations' => 'Prononciations',

	// T
	'titre_page_configurer_playht' => 'Configurer les vocalisations',
	'token_titre' => 'Titre du contenu',
	'token_contenu' => 'Texte du contenu',
	'token_date' => 'Date de publication',
	'token_auteurs' => 'Noms des auteurs',
	'token_rubrique' => 'Rubrique parente',
	'token_site_nom' => 'Nom du site',
	'token_site_slogan' => 'Slogan du site',
);
