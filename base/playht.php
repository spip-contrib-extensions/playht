<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Play.ht
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Playht\Base
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function playht_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['vocalisations'] = 'vocalisations';

	// Les traitements
	$interfaces['table_des_traitements']['VOICE']['vocalisations'] = "playht_champ(%s, 'voice')";
	$interfaces['table_des_traitements']['NARRATION_STYLE']['vocalisations'] = "playht_champ(%s, 'narration_style')";
	$interfaces['table_des_traitements']['AUDIO_DURATION']['vocalisations'] = "playht_champ(%s, 'audio_duration')";
	$interfaces['table_des_traitements']['TEMPLATE']['vocalisations'] = "playht_champ(%s, 'template')";
	$interfaces['table_des_traitements']['METAS']['vocalisations'] = 'unserialize(%s)';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function playht_declarer_tables_objets_sql($tables) {

	$tables['spip_vocalisations'] = array(
		'type' => 'vocalisation',
		'principale' => 'oui',
		'field'=> array(
			'id_vocalisation'    => 'bigint(21) NOT NULL',
			'objet'              => 'varchar(255) NOT NULL DEFAULT ""',
			'id_objet'           => 'bigint(21) NOT NULL DEFAULT 0',
			// Données Play.ht
			'titre'              => 'text NOT NULL DEFAULT ""', // Titre dans le dashboard
			'playht_url'         => 'varchar(255) NOT NULL DEFAULT ""', // URL donnée à Play.ht pour identifier l'article
			'playht_id'          => 'varchar(255) NOT NULL DEFAULT ""', // Identifiant contenu donné par Play.ht
			'transcription_id'   => 'varchar(255) NOT NULL DEFAULT ""', // Identifiant audio donné par Play.ht
			'audio_url'          => 'varchar(255) NOT NULL DEFAULT ""', // URL audio donné par Play.ht
			'voice'              => 'varchar(255) NOT NULL DEFAULT ""', // Identifiant de la voix
			'audio_duration'     => 'decimal(9,4)', // Durée en secondes
			'narration_style'    => 'varchar(255) NOT NULL DEFAULT ""', // Style de narration
			'global_speed'       => 'varchar(3) NOT NULL DEFAULT ""', // Vitesse 20<>200 (pas de int, on veut pas de 0)
			'read_along_enabled' => 'varchar(15) NOT NULL DEFAULT ""', // ?
			'hash'               => 'varchar(255) NOT NULL DEFAULT ""', // Empreinte du texte utilisé pour l'audio
			'sync'               => 'int(1) NOT NULL DEFAULT 0', // Indique si l'empreinte est synchro
			'template'           => 'varchar(255) NOT NULL DEFAULT ""', // Template utilisé
			'metas'              => 'text NOT NULL DEFAULT ""', // Données sérialisées retournées par Play.ht
			// Suite données lambda
			'date'               => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'lang'               => 'varchar(10) NOT NULL DEFAULT ""',
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'          => 'id_vocalisation',
			'KEY objet'            => 'objet',
			'KEY id_objet'         => 'id_objet',
			'KEY transcription_id' => 'transcription_id',
			'KEY playht_id'        => 'playht_id',
			'KEY lang'             => 'lang',
			'KEY statut'           => 'statut',
		),
		'titre' => 'titre AS titre, lang AS lang',
		'date' => 'date',
		'champs_editables'  => array('objet', 'id_objet', 'titre', 'playht_url', 'playht_id', 'transcription_id', 'audio_url', 'voice', 'audio_duration', 'narration_style', 'global_speed', 'read_along_enabled', 'hash', 'sync', 'template', 'metas'),
		'champs_versionnes' => array('objet', 'id_objet', 'titre', 'playht_url', 'playht_id', 'transcription_id', 'audio_url', 'voice', 'audio_duration', 'narration_style', 'global_speed', 'read_along_enabled', 'hash', 'sync', 'template', 'metas'),
		'rechercher_champs' => array('titre' => 10, 'transcription_id' => 10),
		'tables_jointures'  => array(),
		'statut_textes_instituer' => array(
			'conversion'  => 'vocalisation:texte_statut_conversion', // En cours de conversion
			'erreur'      => 'vocalisation:texte_statut_erreur', // Conversion en erreur
			// 'prepa'       => 'vocalisation:texte_statut_prepa', // Brouillon
			'publie'      => 'vocalisation:texte_statut_publie', // Conversion finalisée
			'poubelle'    => 'texte_statut_poubelle', // Poubelle la vie
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'vocalisation:texte_changer_statut_vocalisation',
	);

	return $tables;
}
